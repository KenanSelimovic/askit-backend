// @flow
"use strict";
export type GlobalConfigType = {|
	static_dir: string,
	users: {
		password_min_len: number,
		password_max_len: number,
		first_name_min_len: number,
		first_name_max_len: number,
		last_name_min_len: number,
		last_name_max_len: number,
		image_max_size_bytes: number,
		top_users_per_page: number
	},
	questions: {
		text_min_len: number,
		text_max_len: number,
		questions_per_page: number,
		hot_questions_days_range: number,
		hot_questions_display_num: number,
		search_text_min_len: number,
		search_text_max_len: number
	},
	answers: {
		text_min_len: number,
		text_max_len: number,
		answers_per_page: number
	},
	elasticSearch: {
		url: string,
		refresh_interval: number
	}
|};

const defaultEnvironment = "development";
const environment = process.env.NODE_ENV || defaultEnvironment;
const globalConfig: GlobalConfigType = require("../config.json")[environment];

module.exports.environment = environment;
module.exports.config = globalConfig;
module.exports.jwt_secret = process.env.JWT_SECRET;
module.exports.baseUrl = `http://127.0.0.1:${process.env.PORT || 3000}`;
module.exports.elasticSearchUrl = process.env.ELASTIC_SEARCH_URL || "localhost:9200";
module.exports.staticDir = process.env.STATIC_DIR || globalConfig.static_dir;