// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const usersConfig = require("../../../../config/config.js").config.users;
const {
	validateEmail,
	validatePassword,
	validateText
} = require("../../../../helpers/validations/validations");
const hashPassword = require("../../../auth/methods/hashPassword");
const dateToDbString = require("../../../../helpers/db/dateToDatabaseString");
const registerUser = require("../../methods/registerUser");

// flow types
import type {UserInsertionDataType} from "../../types/auth.flow";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type RegistrationBodyType = {
	data: UserInsertionDataType
}
declare class RegistrationRequest extends Request {
	body: RegistrationBodyType
}

const validateRequest = (body: RegistrationBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		email,
		password,
		first_name,
		last_name
	} = data;
	const {
		password_min_len,
		password_max_len,
		first_name_min_len,
		first_name_max_len,
		last_name_min_len,
		last_name_max_len
	} = usersConfig;
	if (!validateEmail(email)) {
		return ("INVALID_EMAIL");
	}
	if (!validatePassword(password, password_min_len, password_max_len)) {
		return ("INVALID_PASSWORD");
	}
	if (first_name && !validateText(first_name, first_name_min_len, first_name_max_len)) {
		return (`INVALID_FIRST_NAME`);
	}
	if (last_name && !validateText(last_name, last_name_min_len, last_name_max_len)) {
		return (`INVALID_LAST_NAME`);
	}
	return null;
};

/**
 * @api {post} /v1/register Register
 * @apiDescription Main users registration endpoint
 * @apiVersion 1.0.0
 * @apiName Register
 * @apiGroup Auth
 *
 * @apiParam {Object} data Registration data.
 * @apiParam {string} data.email User's email.
 * @apiParam {string} data.password Password in clear format.
 * @apiParam {string} [data.first_name] User's first name
 * @apiParam {string} [data.last_name] User's last name
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data.id Newly created user's id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		id: 604
 *     	}
 *     }
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500 Unknown Server Error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 * @apiError InvalidEmail Invalid email format received, for example string "abc"
 *
 * @apiErrorExample InvalidEmail:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid email received",
 *       "status": 422,
 *       "code": 1,
 *       "data": null
 *     }
 *
 * @apiError DuplicateEmail Received email is already associated with a user
 *
 * @apiErrorExample DuplicateEmail:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Email already exists in database",
 *       "status": 422,
 *       "code": 2,
 *       "data": null
 *     }
 *
 * @apiError InvalidPassword Received password is not in valid format (eg. too short)
 *
 * @apiErrorExample InvalidPassword:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid password received",
 *       "status": 422,
 *       "code": 3,
 *       "data": null
 *     }
 *
 * @apiError InvalidFirstName Received first name is not in valid format
 *
 * @apiErrorExample InvalidFirstName:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid first name received",
 *       "status": 422,
 *       "code": 4,
 *       "data": null
 *     }
 *
 * @apiError InvalidLastName Received last name is not in valid format
 *
 * @apiErrorExample InvalidLastName:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid last name received",
 *       "status": 422,
 *       "code": 5,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
async function register(req: RegistrationRequest, res: express$Response, next: express$NextFunction): Promise<void> {
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		email,
		password,
		first_name,
		last_name
	} = body.data;

	const userData: UserInsertionDataType = {
		email,
		password,
		first_name,
		last_name
	};
	let registrationData;
	try {
		registrationData = await registerUser(userData);
		if (!registrationData) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (registrationData.error !== null) {
			closeRequest.genericError(res, registrationData.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		id: registrationData.values.id,
		token: registrationData.values.token,
	};
	closeRequest.success(res, returnData);
}
module.exports = register;
