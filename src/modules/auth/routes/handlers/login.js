// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateEmail,
	validatePassword
} = require("../../../../helpers/validations/validations");
const checkCredentials = require("../../methods/checkCredentials");
const usersConfig = require("../../../../config/config.js").config.users;

// flow types
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type LoginRequestBodyType = {
	data: {
		email: string,
		password: string
	}
};
declare class LoginRequestType extends Request {
	body: LoginRequestBodyType
}

const validateRequest = (body: LoginRequestBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		password_min_len,
		password_max_len
	} = usersConfig;
	const {
		email,
		password
	} = data;
	if (!validateEmail(email)) {
		return ("INVALID_EMAIL");
	}
	if (!validatePassword(password, password_min_len, password_max_len)) {
		return ("INVALID_PASSWORD");
	}
	return null;
};

/**
 * @api {post} /v1/auth/login Login
 * @apiDescription Main users login
 * @apiVersion 1.0.0
 * @apiName Login
 * @apiGroup Auth
 *
 * @apiParam {Object} data Login data
 * @apiParam {string} data.email User's email.
 * @apiParam {string} data.password Password in clear format.
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data Data containing token and user id
 * @apiSuccess {Object} data.token Authorization token
 * @apiSuccess {Object} data.id Id of the user
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		token: 'eyJ0eXAiOiJKV1QiLC9.eyJzdWIiOjY1LCJpYXQiOj9.v-hodqolO2LbVDIvlW1OUGUeN3k5ffnneM',
 *     		id: 1
 *     	}
 *     }
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 * @apiError InvalidEmail Invalid email format received, for example string "abc"
 *
 * @apiErrorExample InvalidEmail:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid email received",
 *       "status": 422,
 *       "code": 1,
 *       "data": null
 *     }
 *
 * @apiError IncorrectLoginCredentials Email and password don't match any user
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Invalid login credentials",
 *       "status": 401,
 *       "code": null,
 *       "data": null
 *     }
 */
// eslint-disable-next-line max-len
module.exports = async (req: LoginRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.invalidRequest(res, error);
		return;
	}

	const {
		email,
		password
	} = req.body.data;

	let credentialsValidationResponse;
	try {
		credentialsValidationResponse = await checkCredentials(email, password)
	}
	catch (err) {
		next(err);
		return;
	}
	if (credentialsValidationResponse.error !== null) {
		closeRequest.genericError(res, credentialsValidationResponse.error);
		return;
	}
	const dataToReturn = {
		id: credentialsValidationResponse.values.id,
		token: credentialsValidationResponse.values.token
	};
	closeRequest.success(res, dataToReturn);
}
