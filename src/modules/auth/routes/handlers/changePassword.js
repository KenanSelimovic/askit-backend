// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const changePassword = require("../../methods/changePassword");
const usersConfig = require("../../../../config/config.js").config.users;
const {
	validateEmail,
	validatePassword
} = require("../../../../helpers/validations/validations");
// flow types
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
import type {
	UserType
} from "../../../users";

type ChangePasswordBodyType = {
	data: {
		old_password: string,
		new_password: string
	}
}
declare class UpdateProfileRequestType extends express$Request {
	user: UserType,
	body: ChangePasswordBodyType
}

const validateRequest = (body: ChangePasswordBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		old_password,
		new_password
	} = data;
	const {
		password_min_len,
		password_max_len
	} = usersConfig;
	if (!validatePassword(old_password, password_min_len, password_max_len)) {
		return ("INVALID_PASSWORD");
	}
	if (!validatePassword(new_password, password_min_len, password_max_len)) {
		return ("INVALID_PASSWORD");
	}
	return null;
};

/**
 * @api {post} /v1/auth/password Change password
 * @apiDescription Change user password
 * @apiVersion 1.0.0
 * @apiName Change password
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "a8313a5728bd419.a424e4dd6e503bc2ed73f08e9fe.7aa8f72e396970fdb49" }
 * 
 *
 * @apiParam {Object} data Update data.
 * @apiParam {string} data.old_password Current password.
 * @apiParam {string} data.new_password New password
 *
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success responses
 * @apiSuccess {Object} data  Data is null
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *  	 	"status": 200,
 *			"message": "success",
 * 			"code": null,
 * 			"data": null
 * 	}
 *
 *
 * @apiError InvalidPassword Received password is not in valid format (eg. too short)
 *
 * @apiErrorExample InvalidPassword:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid password received",
 *       "status": 422,
 *       "code": 3,
 *       "data": null
 *     }
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 * @apiError NotAuthorized Authorization token is invalid
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *   	 "status": 401,
 *		 "message": "Not authorized",
 * 		 "code": null,
 *		 "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: UpdateProfileRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const userId = req.user.id;
	const error = validateRequest(req.body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		old_password,
		new_password
	} = req.body.data;
	
	let passwordChangeData;
	try {
		passwordChangeData = await changePassword(
			userId,
			req.user.email,
			old_password,
			new_password
		);
	}
	catch (err) {
		next(err);
		return;
	}
	if (passwordChangeData.error !== null) {
		closeRequest.genericError(res, passwordChangeData.error);
		return;
	}
	closeRequest.success(res);
}
