// @flow
"use strict";
const login = require("./handlers/login");
const register = require("./handlers/register");
const changePassword = require("./handlers/changePassword");

const requireAuth = require("../methods/requireAuth");

module.exports = (app: express$Application) => {
	app.post("/v1/auth/login", login);
	app.post("/v1/auth/register", register);
	app.post("/v1/auth/password", requireAuth, changePassword);
};
