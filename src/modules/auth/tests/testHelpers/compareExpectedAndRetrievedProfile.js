// @flow
"use strict";
const compareExpectedAndRetrieved
	= require("../../../../helpers/testHelpers/compareExpectedAndRetrieved");

type DifferencesResultType = {
	valid: boolean,
	message: ?string
};


module.exports = (expected: Object, received: Object): DifferencesResultType => {
	const fieldsToCheck = [
		"first_name",
		"last_name",
		"email"
	];
	const { valid: mainDataValid, message } = compareExpectedAndRetrieved(expected, received, fieldsToCheck);
	if (!mainDataValid) {
		return { valid: mainDataValid, message };
	}
	return ({valid: true, message: null});
};