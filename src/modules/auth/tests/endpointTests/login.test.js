// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

// flow types
type AuthenticationResponseType = {
	data: {
		id: number
	},
	message: string,
	code: ?number,
	status: number
};
import type {UserInsertionDataType} from "../../types/auth.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const compareExpectedAndRetrievedProfile
	= require("../testHelpers/compareExpectedAndRetrievedProfile");
// const loginAndGetProfile
// 	= require("../../../helpers/testHelpers/loginAndGetProfile");

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Valid data passes for login", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validRegistrationCredentials})
	});
	// now login
	const credentials = {
		email: validRegistrationCredentials.email,
		password: validRegistrationCredentials.password
	};
	const loginResult = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: credentials})
	});
	t.equal(registrationResult.status, 200, "Response passes");	
	const parsedResponse = await loginResult.json();
	t.equal(parsedResponse.status, 200, "Status 200 is returned");
	t.ok(parsedResponse.data, "Data is returned");

	const {
		token,
		id: userId
	} = parsedResponse.data;
	t.ok(userId, "user id is returned");
	t.ok(token, "token id is returned");

	t.end();
});
test("Invalid data returns 401", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validRegistrationCredentials})
	});
	// now login, but with incorrect credentials
	const incorrectCredentials = {
		email: validRegistrationCredentials.email,
		password: validRegistrationCredentials.password + "1"
	};
	const loginResult = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: incorrectCredentials})
	});
	t.equal(loginResult.status, 401, "Response fails");	
	const parsedResponse = await loginResult.json();
	t.equal(parsedResponse.status, 401, "Status 401 is returned");
	t.notOk(parsedResponse.data, "Data is not");

	t.end();
});
