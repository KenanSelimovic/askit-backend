// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

// flow types
type AuthenticationResponseType = {
	data: {
		id: number
	},
	message: string,
	code: ?number,
	status: number
};
import type {UserInsertionDataType} from "../../types/auth.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const compareExpectedAndRetrievedProfile
	= require("../testHelpers/compareExpectedAndRetrievedProfile");
// const loginAndGetProfile
// 	= require("../../../helpers/testHelpers/loginAndGetProfile");

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Valid data passes for registration", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validRegistrationCredentials})
	});
	t.equal(registrationResult.status, 200, "Response passes");	
	
	const parsedResponse = await registrationResult.json();
	const {
		email,
		first_name,
		last_name
	} = validRegistrationCredentials;
	t.equal(parsedResponse.status, 200, "returns status 200");
	t.ok(parsedResponse.data, "data object exists in response");
	const {id: userId} = parsedResponse.data;
	t.ok(userId, "user id is returned");
	// TODO: retrieve profile and check if matches
	t.end();
});
test("First name is optional", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const dataWithoutFirstName = {
		last_name: validRegistrationCredentials.last_name,
		email: validRegistrationCredentials.email,
		password: validRegistrationCredentials.password
	};
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: dataWithoutFirstName})
	});
	t.equal(registrationResult.status, 200, "Response passes");	
	
	const parsedResponse = await registrationResult.json();
	const {
		email,
		first_name,
		last_name
	} = validRegistrationCredentials;
	t.equal(parsedResponse.status, 200, "returns status 200");
	t.end();
});
test("Last name is optional", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const dataWithoutLastName = {
		first_name: validRegistrationCredentials.first_name,
		email: validRegistrationCredentials.email,
		password: validRegistrationCredentials.password
	};
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: dataWithoutLastName})
	});
	t.equal(registrationResult.status, 200, "Response passes");	
	
	const parsedResponse = await registrationResult.json();
	const {
		email,
		first_name,
		last_name
	} = validRegistrationCredentials;
	t.end();
});
