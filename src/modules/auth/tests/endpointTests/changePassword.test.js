// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

// flow types
type PasswordChangeResponseType = {|
	data: null,
	message: string,
	code: null,
	status: 200
|}
|
{|
	data: null,
	message: string,
	code: number,
	status: number
|};
import type {UserInsertionDataType} from "../../types/auth.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const compareExpectedAndRetrievedProfile
	= require("../testHelpers/compareExpectedAndRetrievedProfile");

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Password change endpoint changes password if sent valid data", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validRegistrationCredentials})
	});
	const parsedRegistrationResponse = await registrationResult.json();
	const {token} = parsedRegistrationResponse.data;

	// now change password
	const newPassword = chance.word({length: 10});
	const changePasswordData = {
		old_password: validRegistrationCredentials.password,
		new_password: newPassword
	};
	const passwordChangeResult = await fetch(`${baseUrl}/v1/auth/password`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: changePasswordData})
	});
	t.equal(passwordChangeResult.status, 200, "Request passes");
	const parsedResponse = await passwordChangeResult.json();
	t.equal(parsedResponse.status, 200, "Status 200 is returned");
	t.equal(parsedResponse.data, null, "No data is returned");

	// now try using new password
	const credentials = {
		email: validRegistrationCredentials.email,
		password: newPassword
	};
	const loginResult = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: credentials})
	});
	t.equal(loginResult.status, 200, "Response passes");	
	t.end();
});
test("Invalid old password returns status 401", async (t: tape$Context) => {
	const validRegistrationCredentials = generateRandomProfileData();
	const registrationResult = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validRegistrationCredentials})
	});
	const parsedRegistrationResponse = await registrationResult.json();
	const {token} = parsedRegistrationResponse.data;

	// now change password
	const newPassword = chance.word({length: 10});
	const changePasswordData = {
		old_password: "testtest",
		new_password: newPassword
	};
	const passwordChangeResult = await fetch(`${baseUrl}/v1/auth/password`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: changePasswordData})
	});
	t.equal(passwordChangeResult.status, 401, "Request fails");
	const parsedResponse = await passwordChangeResult.json();
	t.equal(parsedResponse.status, 401, "Status 401 is returned");

	// now try using new password (should fail)
	const invalidCredentials = {
		email: validRegistrationCredentials.email,
		password: newPassword
	};
	const newPasswordLoginResult = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: invalidCredentials})
	});
	t.equal(newPasswordLoginResult.status, 401, "Login with new password fails");

	// now try using old password (should work)
	const validCredentials = {
		email: validRegistrationCredentials.email,
		password: validRegistrationCredentials.password
	};
	const oldPasswordLoginResult = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: validCredentials})
	});
	t.equal(oldPasswordLoginResult.status, 200, "Login with old password works");
	t.end();
});
