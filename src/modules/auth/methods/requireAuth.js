// @flow
"use strict";
const passport = require("passport");
const {notAuthorized} = require("../../../helpers/http/responseStandardizer");
import type {UserType} from "../../users/";
require("../lib/passport/jwtStrategy");

// eslint-disable-next-line immutable/no-mutation
module.exports = (req: express$Request, res: express$Response, next: express$NextFunction) => {
	passport.authenticate("jwt", {session: false}, (err: Error, user: UserType) => {
		if (!user) {
			notAuthorized(res);
			return;
		}
		req.user = user;
		next();
	})(req, res, next);
};
