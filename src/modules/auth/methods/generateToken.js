// @flow
"use strict";
const jwt = require("jwt-simple");
const {crashLogger} = require("../../logging/");

module.exports = (userId: number): string => {
	if (!userId) {
		crashLogger.log(
			"error",
			new Error("Trying to generate user jwt token without supplying an id")
		);
	}
	const timestamp = new Date().getTime();
	return jwt.encode({sub: userId, iat: timestamp}, process.env.JWT_SECRET);
};
