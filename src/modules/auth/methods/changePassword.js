// @flow
"use strict";
const updatePassword = require("../db/updatePassword");
const {crashLogger} = require("../../logging");
const {
	validateNumber,
	validatePassword,
	validateEmail
} = require("../../../helpers/validations/validations");
const {getUserByEmail} = require("../../users/");
const checkCredentials = require("./checkCredentials");
const hashPassword = require("./hashPassword");

/*
 * Flow types
 */
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type ChangePasswordReturnType = {|
	values: {},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (
	userId: number,
	email: string,
	oldPassword: string,
	newPassword: string
): Promise<ChangePasswordReturnType> => {
	// validate received data
	const validUserId = validateNumber(userId);
	const validEmail = validateEmail(email);
	const validPassword = validatePassword(oldPassword);
	const validNewPassword = validatePassword(newPassword);
	if (!validUserId || !validEmail || !validPassword || !validNewPassword) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if old password is valid
	let credentialsValidationData;
	try {
		credentialsValidationData = await checkCredentials(
			email,
			oldPassword
		);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (credentialsValidationData.error === "NOT_AUTHORIZED") {
		return ({
			error: "NOT_AUTHORIZED",
			values: null
		});
	}
	if (credentialsValidationData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed validating credentials: ${JSON.stringify({email, oldPassword})}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	// everything ok, update
	const hashedPassword = await hashPassword(newPassword);
	try {
		await updatePassword(userId, hashedPassword);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {},
		error: null
	});
};
