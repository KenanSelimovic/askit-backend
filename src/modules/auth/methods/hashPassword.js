// @flow
"use strict";
const bcrypt = require("bcrypt");
const {crashLogger} = require("../../logging");

module.exports = (password: string): Promise<string> => {
	return new Promise((resolve, reject) => {
		bcrypt.hash(password, 10, function(err, hash) {
			if (err) {
				crashLogger.log("error", err);
				reject(err);
			}
			resolve(hash);
		});
	});
};
