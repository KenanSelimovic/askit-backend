// @flow
"use strict";
const Joi = require("joi");
const createUser = require("../db/createUser");
const {crashLogger} = require("../../logging");
const {userInsertionDataSchema} = require("../types/validationSchemas");
const {getUserByEmail} = require("../../users");
const hashPassword = require("./hashPassword");
const generateToken = require("./generateToken");

/*
 * Flow types
 */
import type {UserInsertionDataType} from "../types/auth.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type RegistrationResultType = {|
	error: null,
	values: {
		id: number,
		token: string
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

/*
 * Main exported method
 */

module.exports = async (userData: UserInsertionDataType): Promise<RegistrationResultType> => {
	// validate received shape
	if (Joi.validate(userData, userInsertionDataSchema).error) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user with same email already exists
	const sameEmailData = await getUserByEmail(userData.email);
	if (sameEmailData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user by email: ${JSON.stringify(sameEmailData.error)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (sameEmailData.values.user) {
		return ({
			error: "DUPLICATE_EMAIL",
			values: null
		});
	}
	// hash the password
	let hashedPassword: string;
	try {
		hashedPassword = await hashPassword(userData.password);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	const dataWithHashedPassword = Object.assign(
		{},
		userData,
		{
			password: hashedPassword
		}
	);
	// try inserting the new user	
	let createdUserId: ?number;
	try {
		createdUserId = await createUser(dataWithHashedPassword);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (!createdUserId) {
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	const token: string = generateToken(createdUserId);
	return ({
		error: null,
		values: {
			id: createdUserId,
			token
		}
	});
}