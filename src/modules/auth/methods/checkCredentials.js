// @flow
"use strict";
const {
	validateText,
	validatePassword
} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const {getUserByEmail} = require("../../users/");
const getUserPassword = require("../db/getUserPassword");
const Joi = require("joi");
const bcrypt = require("bcrypt");
const generateToken = require("./generateToken");

// flow types
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type CredentialsResultType = {|
	error: null,
	values: {
		token: string,
		id: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (email: string, password: string): Promise<CredentialsResultType> => {
	if (!validateText(email) || !validatePassword(password)) {
		return ({
			values: null,
			error: "INVALID_SHAPE"
		});
	}

	// get user with requested email
	const userData = await getUserByEmail(email);
	if (userData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user by email: ${JSON.stringify(userData.error)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (!userData.values.user) {
		return ({
			error: "NOT_AUTHORIZED",
			values: null
		});
	}
	const {user} = userData.values;

	// get user's password and compare
	const passwordInDb = await getUserPassword(user.id);
	let isMatch: boolean;
	try {
		isMatch = await bcrypt.compare(password, passwordInDb);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (!isMatch) {
		return ({
			error: "NOT_AUTHORIZED",
			values: null
		});
	}

	// everything ok, generate login token
	const token: string = generateToken(user.id);
	if (!token) {
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	return ({
		error: null,
		values: {
			token,
			id: user.id
		}
	});
};
