// @flow
"use strict";

export type UserInsertionDataType = {
	first_name: string,
	last_name: string,
	email: string,
	password: string
};
