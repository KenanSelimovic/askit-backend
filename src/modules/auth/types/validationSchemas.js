// @flow
"use strict";
const Joi = require("joi");
const usersConfig = require("../../../config/config").config.users;

const {
	first_name_min_len,
	first_name_max_len,
	last_name_min_len,
	last_name_max_len,
	password_min_len,
	password_max_len
} = usersConfig;
const userInsertionData = Joi.object().keys({
	first_name: Joi.string().min(first_name_min_len).max(first_name_max_len).optional().allow([""]),
	last_name: Joi.string().min(last_name_min_len).max(last_name_max_len).optional().allow([""]),
	email: Joi.string().email(),
	password: Joi.string().min(password_min_len).max(password_max_len)
});

module.exports.userInsertionDataSchema = userInsertionData;
