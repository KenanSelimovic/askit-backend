// @flow
"use strict";
const db = require("../../db/dbConnection");

module.exports = async (userId: number, newPassword: string): Promise<void> => {
	const mainQuery = `
		UPDATE
			users
				SET
					password=$1
						WHERE id=$2
	`;
	await db.exec(mainQuery, [
		newPassword,
		userId
	]);
};
