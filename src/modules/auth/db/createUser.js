// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {UserInsertionDataType} from "../types/auth.flow";

module.exports = async (data: UserInsertionDataType): Promise<?number> => {
	const mainQuery = `
			INSERT INTO
				users (
					email,
					password,
					first_name,
					last_name)
					VALUES(
						lower($1),
						$2,
						$3,
						$4)
						RETURNING users.id;
		`;
	const userResult = await db.exec(mainQuery, [
		data.email,
		data.password,
		data.first_name,
		data.last_name
	]);
	const insertedUserId = userResult.rows[0].id;
	if (!insertedUserId) {
		return null;
	}
	return insertedUserId;
};
