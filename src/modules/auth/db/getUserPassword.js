// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {UserInsertionDataType} from "../types/auth.flow";

module.exports = async (userId: number): Promise<?string> => {
	const mainQuery = `
			SELECT
				password
					FROM
						users
							WHERE
								id=$1
		`;
	const userResult = await db.exec(mainQuery, [
		userId
	]);
	const userData = userResult.rows[0];
	if (!userData) {
		return null;
	}
	return userData.password || null;
};
