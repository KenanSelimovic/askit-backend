// @flow
"use strict";
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const passport = require("passport");
const db = require("../../../db/dbConnection");
const {crashLogger} = require("../../../logging/index");

// flow types
import type {UserType} from "../../../users";
type DecodedJwtPayloadType = {
	sub: number
};
type DoneFunctionType = (e: ?Error, user: ?UserType, info: ?string) => void;

// setup options for jwt strategy
const jwtOptions = {
	jwtFromRequest: ExtractJwt.fromHeader("authorization"),
	secretOrKey: process.env.JWT_SECRET
};

// create jwt strategy
const jwtLogin = new JwtStrategy(
	jwtOptions,
	async (
		payload: DecodedJwtPayloadType,
		done: DoneFunctionType
	): Promise<void> => {
		try {
			const userResult = await db.exec(`
			SELECT
				id,
				first_name,
				last_name,
				email
					FROM
						users
							WHERE
								id=$1`, [payload.sub]);
			const user = userResult.rows[0];
			if (user) {
				done(null, user);
			}
			else {
				done(null, null);
			}
		}
		catch (err) {
			crashLogger.log("error", err);
			done(err, null);
		}
	});

// tell passport to use this strategy
passport.use(jwtLogin);
