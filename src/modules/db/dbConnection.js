// @flow
"use strict";
const pg = require('pg');
const {crashLogger, dbLogger} = require("../logging");

// this initializes a connection pool
// it will keep idle connections open for 30 seconds
// and set a limit of maximum 10 idle clients
const pool = new pg.Pool();

pool.on('error', err => {
	crashLogger.log("error", err);
});

// eslint-disable-next-line max-len, immutable/no-mutation
module.exports.exec = <T>(queryString: string, values: Array<mixed>): Promise<{rows: Array<T>}> => {
	return new Promise((resolve, reject) => {
		dbLogger.log("info", {timestamp: new Date().getTime(), query: queryString, values});
		pool.query(queryString, values, (err, res) => {
			if (err) {
				crashLogger.log("error", err);
				crashLogger.log(
					"error",
					`\nnext query crashed: ${queryString}\n${JSON.stringify(values)}`
				);
				reject(err);
			}
			if (!res) {
				reject(
					new Error(
						`No data for query string: ${queryString}, values: ${JSON.stringify({values})}`
					)
				);
			}
			resolve(res);
		});
	});
};

// the pool also supports checking out a client for
// multiple operations, such as a transaction
// eslint-disable-next-line immutable/no-mutation
module.exports.connect = () => {
	return new Promise((resolve) => {
		resolve();
	});
};
