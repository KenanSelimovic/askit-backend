// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const questionsConfig = require("../../../../config/config.js").config.questions;

module.exports = () => {
	return {
		text: chance.paragraph({sentences: 3}).slice(0, questionsConfig.text_max_len)
	};
};