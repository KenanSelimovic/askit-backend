// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../../../config/config");
const generateRandomQuestion = require("./generateRandomQuestion");

module.exports = async (token: string): Promise<number> => {
	const questionsData = generateRandomQuestion();

	const creationResponse = await fetch(`${baseUrl}/v1/questions`, {
		method: "POST",
		headers: {
			"Content-type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: questionsData})
	})
	const parsedResponseData = await creationResponse.json();
	if (creationResponse.status < 200 || creationResponse.status > 300) {
		throw new Error(`Failed submitting a question: ${JSON.stringify(parsedResponseData)}`);
	}
	return parsedResponseData.data.id;
}