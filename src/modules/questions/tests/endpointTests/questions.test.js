// @flow
"use strict";

require("./createQuestion.test.js");
require("./getLatestQuestions.test.js");
require("./getHotQuestions.test");
require("./getOwnQuestions.test");
require("./getQuestion.test");
require("./voteQuestion.test");
