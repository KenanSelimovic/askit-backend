// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {questionSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");

const before = test;

import type {QuestionInsertionType} from "../../types/questions.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const generateRandomQuestion = require("../testHelpers/generateRandomQuestion");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /questions returns questions array", async (t: tape$Context) => {
	const questionsResponse = await fetch(`${baseUrl}/v1/questions`);
	t.equal(questionsResponse.status, 200, "Response passes");	
	const parsedResponse = await questionsResponse.json();

	t.equal(
		parsedResponse.status,
		200,
		"response json contains status 200"
	);
	t.ok(parsedResponse.data, "data object exists in response");
	t.ok(
		Array.isArray(parsedResponse.data.questions),
		"questions array is returned"
	);
	t.end();
});
test("GET /questions returns proper questions shape", async (t: tape$Context) => {
	const questionsResponse = await fetch(`${baseUrl}/v1/questions`);
	t.equal(questionsResponse.status, 200, "Response passes");	
	const parsedResponse = await questionsResponse.json();
	parsedResponse.data.questions.forEach(question => {
		t.notOk(
			Joi.validate(question, questionSchema).error,
			"Question has proper shape"
		);
	});
	t.pass("Questions are of valid shape");
	t.ok(parsedResponse.data.page, "Page value is returned");
	t.ok(parsedResponse.data.total_pages, "Total pages value is returned");
	t.end();
});
