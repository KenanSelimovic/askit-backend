// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

// flow types
type QuestionCreationResponseType = {
	data: {
		id: number
	},
	message: string,
	code: ?number,
	status: number
};
import type {QuestionInsertionType} from "../../types/questions.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const generateRandomQuestion = require("../testHelpers/generateRandomQuestion");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Question creation requires auth", async (t: tape$Context) => {
	const questionData = generateRandomQuestion();
	const questionCreationResult = await fetch(`${baseUrl}/v1/questions`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: questionData})
	});
	t.equal(questionCreationResult.status, 401, "Response fails");	
	const parsedResponse = await questionCreationResult.json();

	t.equal(
		parsedResponse.status,
		401,
		"response json contains status 401"
	);
	t.notOk(parsedResponse.data, "data object doesn't exist in response");
	t.end();
});
test("Valid question creating passes", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const questionData = generateRandomQuestion();
	const questionCreationResult = await fetch(`${baseUrl}/v1/questions`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: questionData})
	});
	t.equal(questionCreationResult.status, 200, "Response passes");	
	const parsedResponse = await questionCreationResult.json();

	t.equal(parsedResponse.status, 200, "returns status 200");
	t.ok(parsedResponse.data, "data object exists in response");
	const {id: questionId} = parsedResponse.data;
	t.ok(questionId, "Question id is returned");
	t.end();
});