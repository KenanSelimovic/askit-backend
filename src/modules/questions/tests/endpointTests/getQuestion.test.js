// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {questionSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");
const getRandomQuestionId
	= require("../../../../helpers/testHelpers/getRandomQuestionId");

const before = test;

import type {QuestionInsertionType} from "../../types/questions.flow";

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const generateRandomQuestion = require("../testHelpers/generateRandomQuestion");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /question single questions' data", async (t: tape$Context) => {
	const randomQuestionId = await getRandomQuestionId();

	if (!randomQuestionId) {
		// no tests in db
		t.end();
		return;
	}
	const unparsedQuestionResponse = await fetch(`${baseUrl}/v1/questions/${randomQuestionId}`);
	const questionResponse = await unparsedQuestionResponse.json();
	t.equal(
		questionResponse.status,
		200,
		"response json contains status 200"
	);
	t.ok(questionResponse.data, "data object exists in response");
	t.notOk(
		Joi.validate(questionResponse.data.question, questionSchema).error,
		"Question has proper shape"
	);
	t.end();
});
