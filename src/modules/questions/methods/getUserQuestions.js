// @flow
"use strict";

const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getUserQuestions = require("../db/getUserQuestions");

// flow types
import type {QuestionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type QuestionResultType = {|
	error: null,
	values: {
		questions: Array<QuestionType>,
		totalPages: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

// eslint-disable-next-line immutable/no-mutation
module.exports = async (
	userId: number,
	page: number,
	perPage: number
): Promise<QuestionResultType> => {
	if (!validateNumber(userId)) {
		return ({
			error: "INVALID_OBJECT_ID",
			values: null
		});
	}
	if (!validateNumber(page)) {
		return ({
			error: "PAGINATION_INVALID_PAGE",
			values: null
		});
	}
	if (!validateNumber(perPage)) {
		return ({
			error: "PAGINATION_INVALID_PER_PAGE",
			values: null
		});
	}
	
	let questionsData;
	try {
		questionsData = await getUserQuestions(
			userId,
			page,
			perPage
		);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});
	}
	const totalPages = Math.ceil(
		(questionsData.total_count) / perPage
	);
	return ({
		error: null,
		values: {
			totalPages,
			questions: questionsData.questions
		}
	})
};
