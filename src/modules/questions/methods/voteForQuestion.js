// @flow
"use strict";
const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const {getUserById} = require("../../users/");
const {storeVote, updateVote} = require("../../votes/");
const {questionInsertionSchema} = require("../types/validationSchemas");
const {getUserVote} = require("../../votes/");
const getQuestionById = require("../db/getQuestionById");

// flow types
import type {VoteType, VoteInsertionType} from "../../votes/";
import type {QuestionInsertionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type QuestionVoteResultType = {|
	error: null,
	values: {}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (type: VoteType, userId: number, questionId: number): Promise<QuestionVoteResultType> => {
	// validate received data shape
	if (type !== "up" && type !== "down") {
		return ({
			error: "INVALID_VOTE_TYPE",
			values: null
		});
	}

	// check if user trying to vote exists
	let requestedUser;
	try {
		const requestedUserData = await getUserById(userId);
		if (requestedUserData.error !== null) {
			crashLogger.log(
				"error",
				new Error(`Failed retrieving user ${userId}: ${JSON.stringify(requestedUserData)}`)
			)
			return ({
				values: null,
				error: "UNKNOWN_ERROR"
			});	
		}
		if (!requestedUserData.values.user) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// check if user is the owner/author of the question
	let requestedQuestion;
	try {
		requestedQuestion = await getQuestionById(questionId);
		if (!requestedQuestion) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	if (requestedQuestion.user.id === userId) {
		return ({
			values: null,
			error: "FORBIDDEN"
		});
	}

	const vote: VoteInsertionType = {
		user_id: userId,
		type: type,
		post_id: questionId
	};

	// check if the user has already voted
	let voteData;
	try {
		voteData = await getUserVote(userId, questionId);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	if (voteData.error !== null) {
		return ({
			values: null,
			error: voteData.error
		});	
	}
	const existingVote = voteData.values.vote;
	if (existingVote && existingVote.type === type) {
		// there is already the same vote, so we do nothing
		return ({
			values: {},
			error: null
		});
	}
	// user either hasn't voted or has voted differently
	// if there is existing vote, we update
	// if not, we create new 
	try {
		if (existingVote) {
			await updateVote(existingVote.post_id, userId, vote);
		}
		else {
			await storeVote(vote);
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// done, return
	return ({
		values: {},
		error: null
	});
};
