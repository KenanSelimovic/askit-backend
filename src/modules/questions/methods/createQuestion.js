// @flow
"use strict";
const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const {getUserById} = require("../../users/");
const storeQuestion = require("../db/storeQuestion");
const {questionInsertionSchema} = require("../types/validationSchemas");
const Joi = require("joi");
const storeQuestionForSearch = require("./storeQuestionForSearch");

// flow types
import type {QuestionInsertionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type QuestionCreationResultType = {|
	error: null,
	values: {
		id: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (data: QuestionInsertionType): Promise<QuestionCreationResultType> => {
	// validate received data shape
	if (Joi.validate(data, questionInsertionSchema).error) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user in question.user_id exists
	let requestedUser;
	try {
		const requestedUserData = await getUserById(data.user_id);
		if (requestedUserData.error !== null) {
			crashLogger.log(
				"error",
				new Error(`Failed retrieving user ${data.user_id} by id: ${JSON.stringify(requestedUserData)}`)
			)
			return ({
				values: null,
				error: "UNKNOWN_ERROR"
			});	
		}
		if (!requestedUserData.values.user) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// store the question
	let storedQuestionId;;
	try {
		storedQuestionId = await storeQuestion(data);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	if (!storedQuestionId) {
		crashLogger.log(
			"error",
			new Error(`Failed storing question: ${JSON.stringify(data)}`)
		)
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// now store question in elastic search data storage
	await storeQuestionForSearch(storedQuestionId);

	return ({
		values: {
			id: storedQuestionId
		},
		error: null
	});
};
