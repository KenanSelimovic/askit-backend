// @flow
"use strict";
const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getQuestionById = require("./getQuestionById");
const storeQuestionForSearch = require("../../elasticSearch/methods/storeQuestionForSearch");

// flow types
import type {QuestionInsertionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type QuestionStorageResultType = {|
	error: null,
	values: {}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (id: number): Promise<QuestionStorageResultType> => {
	if (!validateNumber(id, true)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// retrieve question data so that we can pass it to elasticsearch
	let question;
	try {
		const requestedQuestionData = await getQuestionById(id);
		if (requestedQuestionData.error !== null) {
			crashLogger.log(
				"error",
				new Error(
					`Failed retrieving question ${id}: ${JSON.stringify(requestedQuestionData)}`
				)
			)
			return ({
				values: null,
				error: "UNKNOWN_ERROR"
			});	
		}
		if (!requestedQuestionData.values.question) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
		question = requestedQuestionData.values.question;
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// pass the question data to elastic search module
	let questionStored: boolean;
	try {
		questionStored = await storeQuestionForSearch(question);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	if (!questionStored) {
		crashLogger.log(
			"error",
			new Error(`Failed storing question in elasticsearch: ${JSON.stringify(question)}`)
		)
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	return ({
		values: {
			id: question.id
		},
		error: null
	});
};
