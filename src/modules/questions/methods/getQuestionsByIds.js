// @flow
"use strict";
const getQuestionsByIds = require("../db/getQuestionsByIds");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {QuestionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type QuestionReturnType = {|
	values: {
		questions: Array<QuestionType>
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (ids: Array<number>): Promise<QuestionReturnType> => {
	if (!Array.isArray(ids)) {
		return ({
			values: null,
			error: "INVALID_SHAPE"
		});
	}
	for (const id of ids) {
		if (!validateNumber(id, true)) {
			return ({
				values: null,
				error: "INVALID_OBJECT_ID"
			});
		}
	}
	let questions: Array<QuestionType>;
	try {
		questions = await getQuestionsByIds(ids);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			questions
		},
		error: null
	});
};
