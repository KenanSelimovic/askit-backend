// @flow
"use strict";
const getQuestionById = require("../db/getQuestionById");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {QuestionType} from "../types/questions.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type QuestionReturnType = {|
	values: {
		question: ?QuestionType
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (id: number): Promise<QuestionReturnType> => {
	if (!validateNumber(id, true)) {
		return ({
			values: null,
			error: "INVALID_OBJECT_ID"
		})
	}
	let question: ?QuestionType;
	try {
		question = await getQuestionById(id);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			question
		},
		error: null
	});
};
