// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {QuestionInsertionType} from "../types/questions.flow";

module.exports = async (data: QuestionInsertionType): Promise<?number> => {
	const postInsertionQuery = `
		INSERT
			INTO
				posts(
					created_at
				)
				VALUES(
					current_timestamp
				)
					RETURNING id;
	`;
	const postCreationResult = await db.exec(postInsertionQuery, []);
	const postId = postCreationResult.rows[0].id;
	const mainQuery = `
			INSERT INTO
				questions (
					post_id,
					text,
					user_id)
					VALUES(
						$1,
						$2,
						$3)
						RETURNING questions.post_id;
		`;
	const questionResult = await db.exec(mainQuery, [
		postId,
		data.text,
		data.user_id
	]);
	const insertedQuestionId = questionResult.rows[0].post_id;
	if (!insertedQuestionId) {
		return null;
	}
	return insertedQuestionId;
};
