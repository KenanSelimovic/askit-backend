// @flow
"use strict";
const db = require("../../db/dbConnection");
const generateFullName = require("../../../helpers/generateFullName");

import type {QuestionType} from "../types/questions.flow";


// eslint-disable-next-line immutable/no-mutation
module.exports = async (
	page: number,
	perPage: number
): Promise<{questions: Array<QuestionType>, total_count: number}> => {
	const offset = page * perPage - perPage;
	const countAllQuery = `
		SELECT
			COUNT(*) AS total_count
				FROM
					questions AS Q
						WHERE Q.is_deleted=false;
	`;
	const totalCountResult = await db.exec(countAllQuery, []);
	const totalCount = totalCountResult.rows[0].total_count;
	const mainQuery = `
		SELECT
			Q.post_id AS id,
			Q.text,
			Q.created_at AS time,
			U.id AS user_id,
			COALESCE(U.first_name, '') AS first_name,
			COALESCE(U.last_name, '') AS last_name,
			U.profile_image_url,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=Q.post_id
										AND type=(SELECT id FROM vote_types WHERE name='up')
			) AS votes_up,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=Q.post_id
										AND type=(SELECT id FROM vote_types WHERE name='down')
			) AS votes_down,
			COALESCE(
				(SELECT
					array_to_json(
						array_agg(
							json_build_object(
								'id',  answers.post_id,
								'text', answers.text,
								'time', answers.created_at,
								'user', json_build_object(
									'id', users.id,
									'first_name', users.first_name,
									'last_name', users.last_name,
									'profile_image_url', users.profile_image_url
								),
								'question', json_build_object('id', answers.question_id),
								'votes', json_build_object(
									'up', (
											SELECT COUNT(*) FROM votes AS V
												WHERE V.post_id=answers.post_id AND type=(
													SELECT id FROM vote_types WHERE name='up')
										),
									'down', (
											SELECT COUNT(*) FROM votes AS V
												WHERE V.post_id=answers.post_id AND type=(
													SELECT id FROM vote_types WHERE name='down')
										)	
								)
							) ORDER BY answers.created_at DESC
						)
					)
					FROM answers
						LEFT JOIN users
						ON(answers.user_id=users.id)
							WHERE answers.question_id=Q.post_id
								AND answers.is_deleted=false
				),
				'[]'
			) AS answers
				FROM
					questions AS Q
						LEFT JOIN users AS U
						ON(Q.user_id=U.id)
							WHERE Q.is_deleted=false
								ORDER BY Q.created_at DESC
									LIMIT $1
										OFFSET $2;
	`;
	const questionsResult = await db.exec(mainQuery, [
		perPage,
		offset
	]);
	const questions: Array<QuestionType> = questionsResult.rows.map((questionInDb) => {
		const {
			first_name,
			last_name
		} = questionInDb;
		const full_name = generateFullName(first_name, last_name);
		const question: QuestionType = {
			user: {
				id: questionInDb.user_id,
				full_name: full_name,
				profile_image_url: questionInDb.profile_image_url
			},
			text: questionInDb.text,
			id: questionInDb.id,
			time: questionInDb.time.getTime(),
			votes: {
				up: parseInt(questionInDb.votes_up, 10),
				down: parseInt(questionInDb.votes_down, 10)
			},
			answers: questionInDb.answers.map(answerInDb => {
				return Object.assign(
					{},
					answerInDb,
					{
						time: new Date(answerInDb.time).getTime(),
						user: {
							id: answerInDb.user.id,
							full_name: generateFullName(
								answerInDb.user.first_name,
								answerInDb.user.last_name
							),
							profile_image_url: answerInDb.user.profile_image_url
						}
					}
				)
			})
		};
		return question;
	});
	return ({questions, total_count: totalCount});
};
