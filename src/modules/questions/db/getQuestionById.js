// @flow
"use strict";
const db = require("../../db/dbConnection");
const generateFullName = require("../../../helpers/generateFullName");

// flow types
import type {QuestionType} from "../types/questions.flow";

module.exports = async (id: number): Promise<?QuestionType> => {
	const mainQuery = `
		SELECT
			Q.post_id AS id,
			Q.text,
			Q.created_at AS time,
			U.id AS user_id,
			COALESCE(U.first_name, '') AS first_name,
			COALESCE(U.last_name, '') AS last_name,
			U.profile_image_url,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=Q.post_id
										AND type=(SELECT id FROM vote_types WHERE name='up')
			) AS votes_up,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=Q.post_id
										AND type=(SELECT id FROM vote_types WHERE name='down')
			) AS votes_down,
			COALESCE(
				(SELECT
					array_to_json(
						array_agg(
							json_build_object(
								'id',  answers.post_id,
								'text', answers.text,
								'time', EXTRACT(epoch FROM answers.created_at) * 1000,
								'user', json_build_object(
									'id', users.id,
									'first_name', users.first_name,
									'last_name', users.last_name,
									'profile_image_url', users.profile_image_url
								),
								'question', json_build_object('id', answers.question_id),
								'votes', json_build_object(
									'up', (
											SELECT COUNT(*) FROM votes AS V
												WHERE V.post_id=answers.post_id AND type=(
													SELECT id FROM vote_types WHERE name='up')
										),
									'down', (
											SELECT COUNT(*) FROM votes AS V
												WHERE V.post_id=answers.post_id AND type=(
													SELECT id FROM vote_types WHERE name='down')
										)	
								)
							) ORDER BY answers.created_at DESC
						)
					)
					FROM answers
						LEFT JOIN users
						ON(answers.user_id=users.id)
							WHERE answers.question_id=Q.post_id
								AND answers.is_deleted=false
				),
				'[]'
			) AS answers
				FROM
					questions AS Q
						LEFT JOIN users AS U
						ON(Q.user_id=U.id)
							WHERE Q.post_id=$1
								AND Q.is_deleted=false
									ORDER BY Q.created_at DESC;
	`;
	const questionResult = await db.exec(mainQuery, [id]);

	const questionInDb = questionResult.rows[0];
	if (!questionInDb) {
		return null;
	}

	const {
		first_name,
		last_name
	} = questionInDb;
	const full_name = generateFullName(first_name, last_name);
	const question: QuestionType = {
		user: {
			id: questionInDb.user_id,
			full_name: full_name,
			profile_image_url: questionInDb.profile_image_url
		},
		text: questionInDb.text,
		id: questionInDb.id,
		time: questionInDb.time.getTime(),
		votes: {
			up: parseInt(questionInDb.votes_up, 10),
			down: parseInt(questionInDb.votes_down, 10)
		},
		answers: questionInDb.answers.map(answerInDb => {
			return Object.assign(
				{},
				answerInDb,
				{
					user: {
						id: answerInDb.user.id,
						full_name: generateFullName(
							answerInDb.user.first_name,
							answerInDb.user.last_name
						),
						profile_image_url: answerInDb.user.profile_image_url
					}
				}
			)
		})
	};
	return question;
};
