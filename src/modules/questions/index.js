// @flow
"use strict";

module.exports.routes = require("./routes/");
module.exports.getQuestionById = require("./methods/getQuestionById");
module.exports.getQuestionsByIds = require("./methods/getQuestionsByIds");
