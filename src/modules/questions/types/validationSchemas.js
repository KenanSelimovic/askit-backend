// @flow
"use strict";
const Joi = require("joi");
const {answerSchema} = require("../../answers/types/validationSchemas");
const questionsConfig = require("../../../config/config").config.questions;

const {
	text_min_len,
	text_max_len
} = questionsConfig;
const questionInsertionData = Joi.object().keys({
	user_id: Joi.number().integer().min(1),
	text: Joi.string().min(text_min_len).max(text_max_len)
});
const questionFullData = Joi.object({
	id: Joi.number().integer().min(1).required(),
	text: Joi.string().min(text_min_len).max(text_max_len).required(),
	user: Joi.object({
		id: Joi.number().integer().min(1).required(),
		full_name: Joi.string().min(text_min_len).max(text_max_len).optional().allow([null, ""]),
		profile_image_url: Joi.string().allow([null])
	}).required(),
	votes: Joi.object({
		up: Joi.number().integer().min(0).required(),
		down: Joi.number().integer().min(0).required()
	}).required(),
	time: Joi.date().timestamp('javascript').required(),
	answers: Joi.array().items(answerSchema).required()
});

module.exports.questionInsertionSchema = questionInsertionData;
module.exports.questionSchema = questionFullData;
