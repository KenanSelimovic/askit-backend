// @flow
"use strict";
import type {AnswerType} from "../../answers/";

export type QuestionType = {|
	id: number,
	user: {
		id: number,
		full_name: ?string,
		profile_image_url: ?string
	},
	text: string,
	votes: {
		up: number,
		down: number
	},
	answers: Array<AnswerType>,
	time: number
|};
export type QuestionInsertionType = {|
	user_id: number,
	text: string
|};
