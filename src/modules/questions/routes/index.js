// @flow
"use strict";
const createQuestion = require("./handlers/createQuestion");
const getLatestQuestions = require("./handlers/getLatestQuestions");
const getOwnQuestions = require("./handlers/getOwnQuestions");
const getHotQuestions = require("./handlers/getHotQuestions");
const searchQuestions = require("./handlers/search");
const voteQuestion = require("./handlers/voteQuestion");
const getQuestion = require("./handlers/getQuestion");

const {requireAuth} = require("../../auth");

module.exports = (app: express$Application) => {
	app.post("/v1/questions", requireAuth, createQuestion);
	app.post("/v1/questions/:id/vote", requireAuth, voteQuestion);
	app.get("/v1/questions", getLatestQuestions);
	app.get("/v1/questions/hot", getHotQuestions);
	app.get("/v1/questions/search", searchQuestions);
	app.get("/v1/questions/own", requireAuth, getOwnQuestions);
	app.get("/v1/questions/:id", getQuestion);
};
