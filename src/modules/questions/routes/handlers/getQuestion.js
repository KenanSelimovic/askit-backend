// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateNumber
} = require("../../../../helpers/validations/validations");
const getQuestionById = require("../../methods/getQuestionById");
const questionsConfig = require("../../../../config/config.js").config.questions;

// flow types
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
import type {QuestionType} from "../../types/questions.flow";
declare class GetQuestionRequestType extends Request {
	user: UserType,
	params: {
		id: string
	}
}

/**
* @api {get} /v1/questions/:id Get a question
* @apiDescription Get all of single question's data
* @apiVersion 1.0.0
* @apiName Get a question
* @apiGroup Questions
*
*
* @apiSuccess {String} message  String saying "success"
* @apiSuccess {Number} status  Status 200
* @apiSuccess {Number} code  Code is null for success objects
* @apiSuccess {Object} data  Data object
* @apiSuccess {Object} data.questions  Array of questions
* @apiSuccess {Number} data.question.id Question id
* @apiSuccess {String} data.question.text Question test
* @apiSuccess {Number} data.question.time Time the question was posted (timestamp in milliseconds)
* @apiSuccess {Object} data.question.user User that created the question
* @apiSuccess {Number} data.question.user.id Id of the user that created the question
* @apiSuccess {Number} data.question.user.full_name User's first_name + " " + last_name
* @apiSuccess {Number} data.question.user.profile_image_url Image url (prepend api url to it)
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "message": "success",
*       "status": 200,
*       "code": null,
*       "data": {
*       	"question"":
*				{
*					"id": 1,
*					"text": "Does anyone know...",
*					"user": {
*						"id": 1,
*						"full_name": "Adam Smith",
*						"profile_image_url": "/images/323477gfgfs6423efg62.png"
*					},
*					"time": 1500058687694
*				}
*       }
*     }
*
*
* @apiError InvalidObjectId Invalid question id in url
*
* @apiErrorExample InvalidObjectId:
* 	   HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "Requested object/resource id missing or invalid",
*       "status": 422,
*       "code": 17,
*       "data": null
*     }
* 
*
* @apiError UnknownServerError Some unknown server error occurred
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 500 Internal server error
*     {
*       "message": "Unknown server error",
*       "status": 500,
*       "code": null,
*       "data": null
*     }
*
* @apiError NotAuthorized Authorization token is invalid
*
* @apiErrorExample NotAuthorized:
*     HTTP/1.1 401 Not authorized
*     {
*   	 "status": 401,
*		 "message": "Not authorized",
* 		 "code": null,
*		 "data": null
*     }
*
*/
// eslint-disable-next-line max-len
module.exports = async(req: GetQuestionRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const questionId = parseInt(req.params.id, 10);

	if (!validateNumber(questionId, true)) {
		closeRequest.genericError(res, "INVALID_OBJECT_ID");
		return;
	}
	let questionData;
	try {
		questionData = await getQuestionById(
			questionId
		);
		if (questionData.error !== null) {
			closeRequest.genericError(res, questionData.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		question: questionData.values.question
	};
	closeRequest.success(res, returnData);
}