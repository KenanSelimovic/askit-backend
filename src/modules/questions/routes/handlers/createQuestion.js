// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const questionsConfig = require("../../../../config/config.js").config.questions;
const {
	validateText
} = require("../../../../helpers/validations/validations");
const createQuestion = require("../../methods/createQuestion");

// flow types
import type {QuestionInsertionType} from "../../types/questions.flow";
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type QuestionCreationBodyType = {
	data: {
		text: string
	}
}
declare class QuestionCreationRequest extends Request {
	body: QuestionCreationBodyType,
	user: UserType
}

const validateRequest = (body: QuestionCreationBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		text
	} = data;
	const {
		text_min_len,
		text_max_len
	} = questionsConfig;
	if (!validateText(text, text_min_len, text_max_len)) {
		return ("INVALID_QUESTION_TEXT");
	}
	return null;
};

/**
 * @api {post} /v1/question Create question
 * @apiDescription Endpoint for creating new questions
 * @apiVersion 1.0.0
 * @apiName Create question
 * @apiGroup Questions
 * 
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "4ug54u4nrugyyb4g945n4gugtgtr045ygr45b4brgt" }
 * 
 * @apiParam {Object} data Request data
 * @apiParam {string} data.text Question text
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data.id Newly created question's id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		id: 604
 *     	}
 *     }
 *
 *
 * @apiError NotAuthorized Not authorized for requested action
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Not authorized to execute requested action",
 *       "status": 401,
 *       "code": 16,
 *       "data": null
 *     }
 * 
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: QuestionCreationRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		text
	} = body.data;
	const userId = req.user.id;

	const question: QuestionInsertionType = {
		text,
		user_id: userId
	};
	let createdQuestionId: ?number;
	try {
		const questionData = await createQuestion(question);
		if (!questionData) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (questionData.error !== null) {
			closeRequest.genericError(res, questionData.error);
			return;
		}
		createdQuestionId = questionData.values.id;
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		id: createdQuestionId
	};
	closeRequest.success(res, returnData);
}
