// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateNumber
} = require("../../../../helpers/validations/validations");
const getLatestQuestions = require("../../methods/getLatestQuestions");
const questionsConfig = require("../../../../config/config.js").config.questions;

// flow types
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
import type {QuestionType} from "../../types/questions.flow";
declare class GetQuestionsRequestType extends Request {
	user: ?UserType,
	query: {
		per_page?: string,
		page?: string
	}
}

/**
* @api {get} /v1/questions Get questions
* @apiDescription Get latest questions
* @apiVersion 1.0.0
* @apiName Get questions
* @apiGroup Questions
*
*
* @apiParam {Number} page Pagination page number, default to first page, page num. 1
* @apiParam {Number} per_page Number of questions to return per pagination page
*
* @apiSuccess {String} message  String saying "success"
* @apiSuccess {Number} status  Status 200
* @apiSuccess {Number} code  Code is null for success objects
* @apiSuccess {Object} data  Data object
* @apiSuccess {Object[]} data.questions  Array of questions
* @apiSuccess {Number} data.questions.id Question id
* @apiSuccess {String} data.questions.text Question test
* @apiSuccess {Number} data.questions.time Time the question was posted (timestamp in milliseconds)
* @apiSuccess {Object} data.questions.user User that created the question
* @apiSuccess {Number} data.questions.user.id Id of the user that created the question
* @apiSuccess {Number} data.questions.user.full_name User's first_name + " " + last_name
* @apiSuccess {Number} data.questions.user.profile_image_url Image url (prepend api url to it)
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "message": "success",
*       "status": 200,
*       "code": null,
*       "data": {
*       	"questions"": [
*				{
*					"id": 1,
*					"text": "Does anyone know...",
*					"user": {
*						"id": 1,
*						"full_name": "Adam Smith",
*						"profile_image_url": "/images/323477gfgfs6423efg62.png"
*					},
*					"time": 1500058687694
*				}
*			]
*       }
*     }
*
*
* @apiError InvalidPaginationPage Invalid pagination page received
*
* @apiErrorExample InvalidPaginationPage:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "Page number needs to be a valid positive integer",
*       "status": 422,
*       "code": 9,
*       "data": null
*     }
* @apiError InvalidPerPageValue Invalid number of items per page received
*
* @apiErrorExample InvalidPerPageValue:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "per_page value needs to be a valid positive integer",
*       "status": 422,
*       "code": 10,
*       "data": null
*     }
*
* @apiError UnknownServerError Some unknown server error occurred
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 500 Internal server error
*     {
*       "message": "Unknown server error",
*       "status": 500,
*       "code": null,
*       "data": null
*     }
*
* @apiError NotAuthorized Authorization token is invalid
*
* @apiErrorExample NotAuthorized:
*     HTTP/1.1 401 Not authorized
*     {
*   	 "status": 401,
*		 "message": "Not authorized",
* 		 "code": null,
*		 "data": null
*     }
*
*/
// eslint-disable-next-line max-len
module.exports = async(req: GetQuestionsRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const page = req.query.page ? parseInt(req.query.page, 10) : 1;
	const perPage
		= req.query.per_page
			? parseInt(req.query.per_page, 10)
			: questionsConfig.questions_per_page;

	if (page && !validateNumber(page, true)) {
		closeRequest.genericError(res, "PAGINATION_INVALID_PAGE");
		return;
	}
	if (perPage && !validateNumber(perPage, true)) {
		closeRequest.genericError(res, "PAGINATION_INVALID_PER_PAGE");
		return;
	}
	let questionsData;
	try {
		questionsData = await getLatestQuestions(
			page,
			perPage
		);
		if (questionsData.error !== null) {
			closeRequest.genericError(res, questionsData.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		questions: questionsData.values.questions,
		total_pages: questionsData.values.totalPages,
		page
	};
	closeRequest.success(res, returnData);
}