// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const answersConfig = require("../../../../config/config.js").config.answers;

module.exports = () => {
	return {
		text: chance.paragraph({sentences: 3}).slice(0, answersConfig.text_max_len)
	};
};