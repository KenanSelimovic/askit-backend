// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../../../config/config");
const getRandomQuestionId
	= require("../../../../helpers/testHelpers/getRandomQuestionId");
const generateRandomAnswer = require("./generateRandomAnswer");

module.exports = async (token: string, questionId?: number): Promise<number> => {
	const id = questionId || await getRandomQuestionId();

	if (!id) {
		throw new Error("Question id for answer creation neither found nor provided");
	}
	const answerData = generateRandomAnswer();

	const creationResponse = await fetch(`${baseUrl}/v1/answers/${id}`, {
		method: "POST",
		headers: {
			"Content-type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: answerData})
	})
	const parsedResponseData = await creationResponse.json();
	if (creationResponse.status < 200 || creationResponse.status > 300) {
		throw new Error(`Failed submitting an answer: ${JSON.stringify(parsedResponseData)}`);
	}
	return parsedResponseData.data.id;
}