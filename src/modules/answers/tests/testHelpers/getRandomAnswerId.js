// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../../../config/config");
const createRandomQuestion
	= require("../../../questions/tests/testHelpers/askRandomQuestion");
const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin");
const giveRandomAnswer
	= require("./giveRandomAnswer");

module.exports = async (): Promise<number> => {
	const latestQuestionsResponse = await fetch(`${baseUrl}/v1/questions`);
	const latestQuestionsData = await latestQuestionsResponse.json();
	const latestQuestions = latestQuestionsData.data.questions;

	const questionsWithAnswers = latestQuestions.filter(question => question.answers.length !== 0);
	if (questionsWithAnswers.length > 0) {
		const questionWithAnswers = chance.pickone(questionsWithAnswers);
		return questionWithAnswers.answers[0].id;
	}

	// no question with answers was found, so we create one
	const {token} = await registerAccountAndLogin(generateRandomProfileData());
	const questionId = await createRandomQuestion(token);
	const answerId = giveRandomAnswer(token, questionId);
	return answerId;
}