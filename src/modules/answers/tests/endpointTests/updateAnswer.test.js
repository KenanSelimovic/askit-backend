// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const giveRandomAnswer
	= require("../testHelpers/giveRandomAnswer");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")
const generateRandomAnswer = require("../testHelpers/generateRandomAnswer");

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Answer update requires auth", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const randomAnswerId = await giveRandomAnswer(token);
	const answerUpdateResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}/edit`, {
		method: "POST"
	});

	t.equal(answerUpdateResult.status, 401, "Response fails");

	const parsedResponse = await answerUpdateResult.json();
	t.equal(
		parsedResponse.status,
		401,
		"response json contains status 401"
	);
	t.notOk(parsedResponse.data, "data object doesn't exist in response");

	t.end();
});
test("Valid answer update passes", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const randomAnswerId = await giveRandomAnswer(token);
	const newAnswerData = generateRandomAnswer();

	const answerUpdateResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}/edit`, {
		method: "POST",
		headers: {
			authorization: token,
			"Content-type": "application/json"
		},
		body: JSON.stringify({data: newAnswerData})
	});

	t.equal(answerUpdateResult.status, 200, "Response passes");
	
	const parsedResponse = await answerUpdateResult.json();
	t.equal(parsedResponse.status, 200, "returns status 200");
	t.ok(parsedResponse.data, "data object exists in response");
	const {id: answerId} = parsedResponse.data;
	t.ok(answerId, "Answer id is returned");

	t.end();
});
test("Answer update changes answer text", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const randomAnswerId = await giveRandomAnswer(token);
	const newAnswerData = generateRandomAnswer();
	const answerUpdateResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}/edit`, {
		method: "POST",
		headers: {
			authorization: token,
			"Content-type": "application/json"
		},
		body: JSON.stringify({data: newAnswerData})
	});
	const parsedResponse = await answerUpdateResult.json();
	
	const {id: answerId} = parsedResponse.data;
	const unparsedAnswerRetrievalData = await fetch(`${baseUrl}/v1/answers/${answerId}`);
	const {data: {answer}} = await unparsedAnswerRetrievalData.json();
	t.equal(answer.text,newAnswerData.text, "Answer text is updated");

	t.end();
});