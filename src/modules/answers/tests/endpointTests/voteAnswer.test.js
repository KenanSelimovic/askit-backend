// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const getRandomAnswerId = require("../testHelpers/getRandomAnswerId");

const before = test;

const {baseUrl} = require("../../../../config/config");

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Answer voting requires auth", async (t: tape$Context) => {
	const randomAnswerId = await getRandomAnswerId();
	if (!randomAnswerId) {
		// no answers found
		t.end();
		return;
	}
	const voteData = {
		vote_type: chance.pickone(["up", "down"])
	};
	const submissionUrl = `${baseUrl}/v1/answers/${randomAnswerId}/vote`;
	const votingResult = await fetch(submissionUrl, {
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({data: voteData})
	});
	t.equal(votingResult.status, 401, "Response fails");	
	const parsedResponse = await votingResult.json();

	t.equal(
		parsedResponse.status,
		401,
		"response json contains status 401"
	);
	t.notOk(parsedResponse.data, "data object doesn't exist in response");
	t.end();
});
test("Valid answer vote passes", async (t: tape$Context) => {
	const data = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(data);

	const randomAnswerId = await getRandomAnswerId();
	if (!randomAnswerId) {
		// no answers found
		t.end();
		return;
	}
	const voteData = {
		vote_type: chance.pickone(["up", "down"])
	};
	const submissionUrl = `${baseUrl}/v1/answers/${randomAnswerId}/vote`;
	const votingResult = await fetch(submissionUrl, {
		method: "POST",
		headers: {
			authorization: token,
			"Content-Type": "application/json"
		},
		body: JSON.stringify({data: voteData})
	});
	t.equal(votingResult.status, 200, "Response passes");	
	const parsedResponse = await votingResult.json();

	t.equal(
		parsedResponse.status,
		200,
		"response json contains status 200"
	);
	
	t.end();
});
