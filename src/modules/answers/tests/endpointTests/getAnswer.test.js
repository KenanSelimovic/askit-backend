// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {answerSchema} = require("../../types/validationSchemas");
const Joi = require("joi");

const before = test;

const {baseUrl} = require("../../../../config/config");

const getRandomAnswerId = require("../testHelpers/getRandomAnswerId");

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /answers/id returns valid answer shape", async (t: tape$Context) => {
	const randomAnswerId = await getRandomAnswerId();

	const answerFetchResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}`);

	t.equal(answerFetchResult.status, 200, "Response passes");

	const parsedResponse = await answerFetchResult.json();
	t.equal(
		parsedResponse.status,
		200,
		"response json contains status 200"
	);
	t.notOk(
		Joi.validate(parsedResponse.data.answer, answerSchema).error,
		"Answer has proper shape"
	);

	t.end();
});
