// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const getRandomQuestionId
	= require("../../../../helpers/testHelpers/getRandomQuestionId");
const generateRandomAnswer = require("../testHelpers/generateRandomAnswer");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Answer creation requires auth", async (t: tape$Context) => {
	const randomQuestionId = await getRandomQuestionId();
	if (!randomQuestionId) {
		// no questions in db yet
		t.end();
		return;
	}
	const answer = generateRandomAnswer();
	const answerCreationResult = await fetch(`${baseUrl}/v1/answers/${randomQuestionId}`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: answer})
	});
	t.equal(answerCreationResult.status, 401, "Response fails");	
	const parsedResponse = await answerCreationResult.json();

	t.equal(
		parsedResponse.status,
		401,
		"response json contains status 401"
	);
	t.notOk(parsedResponse.data, "data object doesn't exist in response");
	t.end();
});
test("Valid answer creation passes", async (t: tape$Context) => {
	const randomQuestionId = await getRandomQuestionId();
	if (!randomQuestionId) {
		// no questions in db yet
		t.end();
		return;
	}
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const answer = generateRandomAnswer();
	const anwerCreationResult = await fetch(`${baseUrl}/v1/answers/${randomQuestionId}`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: answer})
	});
	t.equal(anwerCreationResult.status, 200, "Response passes");	
	const parsedResponse = await anwerCreationResult.json();

	t.equal(parsedResponse.status, 200, "returns status 200");
	t.ok(parsedResponse.data, "data object exists in response");
	const {id: answerId} = parsedResponse.data;
	t.ok(answerId, "Answer id is returned");
	t.end();
});