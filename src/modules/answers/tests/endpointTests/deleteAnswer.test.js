// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();

const before = test;

const {baseUrl} = require("../../../../config/config");

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const giveRandomAnswer
	= require("../testHelpers/giveRandomAnswer");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("Answer deletion requires auth", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const randomAnswerId = await giveRandomAnswer(token);
	if (!randomAnswerId) {
		// no answers found
		t.end();
		return;
	}
	const answerDeletionResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}`, {
		method: "DELETE"
	});
	t.equal(answerDeletionResult.status, 401, "Response fails");	
	const parsedResponse = await answerDeletionResult.json();

	t.equal(
		parsedResponse.status,
		401,
		"response json contains status 401"
	);
	t.notOk(parsedResponse.data, "data object doesn't exist in response");
	t.end();
});
test("Valid answer deletion passes", async (t: tape$Context) => {
	const accountData = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(accountData);

	const randomAnswerId = await giveRandomAnswer(token);

	const answerDeletionResult = await fetch(`${baseUrl}/v1/answers/${randomAnswerId}`, {
		method: "DELETE",
		headers: {
			authorization: token
		}
	});
	t.equal(answerDeletionResult.status, 200, "Response passes");	
	const parsedResponse = await answerDeletionResult.json();

	t.equal(parsedResponse.status, 200, "returns status 200");
	t.ok(parsedResponse.data, "data object exists in response");
	const {id: answerId} = parsedResponse.data;
	t.ok(answerId, "Answer id is returned");
	t.end();
});