// @flow
"use strict";

require("./createAnswer.test.js");
require("./voteAnswer.test");
require("./deleteAnswer.test");
require("./updateAnswer.test");
require("./getAnswer.test");
