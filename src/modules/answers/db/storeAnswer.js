// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {AnswerInsertionType} from "../types/answers.flow";

module.exports = async (data: AnswerInsertionType): Promise<?number> => {
	const postInsertionQuery = `
		INSERT
			INTO
				posts(
					created_at
				)
				VALUES(
					current_timestamp
				)
					RETURNING id;
	`;
	const postCreationResult = await db.exec(postInsertionQuery, []);
	const postId = postCreationResult.rows[0].id;
	const mainQuery = `
			INSERT INTO
				answers (
					post_id,
					text,
					user_id,
					question_id)
					VALUES(
						$1,
						$2,
						$3,
						$4)
						RETURNING answers.post_id;
		`;
	const answerResult = await db.exec(mainQuery, [
		postId,
		data.text,
		data.user_id,
		data.question_id
	]);
	const insertedAnswerId = answerResult.rows[0].post_id;
	if (!insertedAnswerId) {
		return null;
	}
	return insertedAnswerId;
};
