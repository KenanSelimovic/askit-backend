// @flow
"use strict";
const db = require("../../db/dbConnection");


module.exports = async (id: number): Promise<?number> => {
	const mainQuery = `
			UPDATE
				answers
					SET
						is_deleted=true
							WHERE post_id=$1
								RETURNING post_id AS id;
		`;
	const answerResult = await db.exec(mainQuery, [
		id
	]);
	const deletedAnswerId = answerResult.rows[0].id;
	if (!deletedAnswerId) {
		return null;
	}
	return deletedAnswerId;
};
