// @flow
"use strict";
const db = require("../../db/dbConnection");
const generateFullName = require("../../../helpers/generateFullName")

// flow types
import type {AnswerType} from "../types/answers.flow";

module.exports = async (id: number): Promise<?AnswerType> => {
	const mainQuery = `
		SELECT
			A.post_id AS id,
			A.user_id,
			A.created_at AS time,
			A.question_id,
			A.text,
			U.first_name AS user_first_name,
			U.last_name AS user_last_name,
			U.profile_image_url AS user_profile_image_url,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=A.post_id
										AND type=(SELECT id FROM vote_types WHERE name='up')
			) AS votes_up,
			(
				SELECT
					COUNT(*)
						FROM
							votes AS V
								WHERE
									V.post_id=A.post_id
										AND type=(SELECT id FROM vote_types WHERE name='down')
			) AS votes_down
				FROM answers AS A
					JOIN users AS U
					ON(A.user_id=U.id)
						WHERE A.post_id=$1
							AND A.is_deleted=false;
	`;
	const questionResult = await db.exec(mainQuery, [id]);

	const answerInDb = questionResult.rows[0];
	if (!answerInDb) {
		return null;
	}

	const answer: AnswerType = {
		user: {
			id: answerInDb.user_id,
			full_name: generateFullName(
				answerInDb.user_first_name,
				answerInDb.user_last_name
			),
			profile_image_url: answerInDb.user_profile_image_url
		},
		text: answerInDb.text,
		id: answerInDb.id,
		time: answerInDb.time.getTime(),
		question: {
			id: answerInDb.question_id
		},
		votes: {
			up: parseInt(answerInDb.votes_up, 10),
			down: parseInt(answerInDb.votes_down, 10)
		}
	};
	return answer;
};
