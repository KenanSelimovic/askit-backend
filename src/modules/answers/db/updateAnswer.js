// @flow
"use strict";
const db = require("../../db/dbConnection");


module.exports = async (id: number, text: string): Promise<?number> => {
	const mainQuery = `
			UPDATE
				answers
					SET
						text=$1
							WHERE post_id=$2
								RETURNING post_id AS id;
		`;
	const answerResult = await db.exec(mainQuery, [
		text,
		id
	]);
	const updatedAnswerId = answerResult.rows[0].id;
	if (!updatedAnswerId) {
		return null;
	}
	return updatedAnswerId;
};
