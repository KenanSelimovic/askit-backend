// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateNumber
} = require("../../../../helpers/validations/validations");
const getAnswerById = require("../../methods/getAnswerById");

// flow types
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
declare class AnswerRequest extends Request {
	user: UserType,
	params: {
		answer_id: string
	}
}


/**
 * @api {get} /v1/answers/:answer_id Get answer by id
 * @apiDescription Endpoint for retrieving single answer
 * @apiVersion 1.0.0
 * @apiName Get anwer
 * @apiGroup Answers
 * 
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data Response data
 * @apiSuccess {Object} data.answer Answer object
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		"answer": {
 *				"user": {
 *					"id": 1785,
 *					"full_name": "Mamie Walters",
 *					"profile_image_url": null
 *				},
 *				"text": "Rohmo kev kineb wu zetius kisok bew ked jozetbif tag ciblurkum ker go. Mitnowi niavo es ike faboru pezfaj nere babelne kip sit koina zocob jewagi ezeraih ipofket nubabvoj olunul su. Docejpag kaoh joj vam vonheg avkomot zav nupke pupnizin je fuzvi ne fi.",
 *				"id": 560,
 *				"time": 1500745287765,
 *				"question": {
 *					"id": 535
 * 				},
 *				"votes": {
 *					"up": 0,
 *					"down": 0
 *				}
 *			}
 *     	}
 *     }
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: AnswerRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	const answerId = parseInt(req.params.answer_id, 10);
	if (!validateNumber(answerId, true)) {
		closeRequest.genericError(res, "INVALID_OBJECT_ID");
		return;
	}
	let requestedAnswer;
	try {
		const answerData = await getAnswerById(answerId);
		if (!answerData) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (answerData.error !== null) {
			closeRequest.genericError(res, answerData.error);
			return;
		}
		requestedAnswer = answerData.values.answer;
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		answer: requestedAnswer
	};
	closeRequest.success(res, returnData);
}
