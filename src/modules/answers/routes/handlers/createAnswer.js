// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const answersConfig = require("../../../../config/config.js").config.answers;
const {
	validateText,
	validateNumber
} = require("../../../../helpers/validations/validations");
const createAnswer = require("../../methods/createAnswer");

// flow types
import type {AnswerInsertionType} from "../../types/answers.flow";
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type AnswerCreationBodyType = {
	data: {
		text: string
	}
}
declare class AnswerCreationRequest extends Request {
	body: AnswerCreationBodyType,
	user: UserType,
	params: {
		question_id: string
	}
}

const validateRequest = (body: AnswerCreationBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		text
	} = data;
	const {
		text_min_len,
		text_max_len
	} = answersConfig;
	if (!validateText(text, text_min_len, text_max_len)) {
		return ("INVALID_QUESTION_TEXT");
	}
	return null;
};

/**
 * @api {post} /v1/answers/:question_id Create answer
 * @apiDescription Endpoint for creating answers to questions
 * @apiVersion 1.0.0
 * @apiName Create anwer
 * @apiGroup Answers
 * 
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "4ug54u4nrugyyb4g945n4gugtgtr045ygr45b4brgt" }
 * 
 * @apiParam {Object} data Request data
 * @apiParam {string} data.text Question text
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data.id Newly created answer's id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		id: 604
 *     	}
 *     }
 *
 *
 * @apiError NotAuthorized Not authorized for requested action
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Not authorized to execute requested action",
 *       "status": 401,
 *       "code": 16,
 *       "data": null
 *     }
 * 
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: AnswerCreationRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	const questionId = parseInt(req.params.question_id, 10);
	if (!validateNumber(questionId, true)) {
		closeRequest.genericError(res, "INVALID_OBJECT_ID");
		return;
	}
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		text
	} = body.data;
	const userId = req.user.id;

	const answer: AnswerInsertionType = {
		text,
		user_id: userId,
		question_id: questionId
	};
	let createdAnswerId: ?number;
	try {
		const answerData = await createAnswer(answer);
		if (!answerData) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (answerData.error !== null) {
			closeRequest.genericError(res, answerData.error);
			return;
		}
		createdAnswerId = answerData.values.id;
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		id: createdAnswerId
	};
	closeRequest.success(res, returnData);
}
