// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const answersConfig = require("../../../../config/config.js").config.answers;
const {
	validateNumber,
	validateText
} = require("../../../../helpers/validations/validations");
const voteForAnswer = require("../../methods/voteForAnswer");

// flow types
import type {VoteType} from "../../../votes/";
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type VoteAnswerBodyType = {
	data: {
		vote_type: VoteType
	}
}
declare class VoteAnswerRequest extends Request {
	body: VoteAnswerBodyType,
	user: UserType,
	params: {
		id: string
	}
}

const validateRequest = (body: VoteAnswerBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		vote_type
	} = data;
	const {
		text_min_len,
		text_max_len
	} = answersConfig;
	if (vote_type !== "up" && vote_type !== "down") {
		return ("INVALID_VOTE_TYPE");
	}
	return null;
};

/**
 * @api {post} /v1/answers/:id/vote Vote for answer
 * @apiDescription Give an answer to a question (a like/dislike)
 * @apiVersion 1.0.0
 * @apiName Vote for answer
 * @apiGroup Answers
 * 
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "4ug54u4nrugyyb4g945n4gugtgtr045ygr45b4brgt" }
 * 
 * @apiParam {Object} data Request data
 * @apiParam {String} data.vote_type Either "up" for like or "down" for dislike
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data Data is null
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: null
 *     }
 *
 *
 * @apiError NotAuthorized Not authorized for requested action
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Not authorized to execute requested action",
 *       "status": 401,
 *       "code": 16,
 *       "data": null
 *     }
 * 
 *
 * @apiError UserIsOwner User cannot vote for own answer
 *
 * @apiErrorExample UserIsOwner:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is not allowed to perform requested action",
 *       "status": 403,
 *       "code": 21,
 *       "data": null
 *     }
 * 
 *
 * @apiError InvalidObjectId Invalid question id in url
 *
 * @apiErrorExample InvalidObjectId:
 * 	   HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Requested object/resource id missing or invalid",
 *       "status": 422,
 *       "code": 17,
 *       "data": null
 *     }
 * 
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: VoteAnswerRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	const answerId = parseInt(req.params.id, 10);
	if (!validateNumber(answerId, true)) {
		closeRequest.genericError(res, "INVALID_OBJECT_ID");
		return;
	}
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		vote_type
	} = body.data;
	const userId = req.user.id;

	let votingResult;
	try {
		votingResult = await voteForAnswer(
			vote_type,
			req.user.id,
			answerId
		);
		if (!votingResult) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (votingResult.error !== null) {
			closeRequest.genericError(res, votingResult.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	closeRequest.success(res);
}
