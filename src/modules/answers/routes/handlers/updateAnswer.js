// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const answersConfig = require("../../../../config/config.js").config.answers;
const {
	validateText,
	validateNumber
} = require("../../../../helpers/validations/validations");
const updateAnswer = require("../../methods/updateAnswer");

// flow types
import type {AnswerInsertionType} from "../../types/answers.flow";
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
type AnswerUpdateBodyType = {
	data: {
		text: string
	}
}
declare class AnswerUpdateRequest extends Request {
	body: AnswerUpdateBodyType,
	user: UserType,
	params: {
		answer_id: string
	}
}

const validateRequest = (body: AnswerUpdateBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		text
	} = data;
	const {
		text_min_len,
		text_max_len
	} = answersConfig;
	if (!validateText(text, text_min_len, text_max_len)) {
		return ("INVALID_QUESTION_TEXT");
	}
	return null;
};

/**
 * @api {post} /v1/answers/:answer_id/edit Update answer
 * @apiDescription Endpoint for updating existing answer
 * @apiVersion 1.0.0
 * @apiName Update anwer
 * @apiGroup Answers
 * 
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "4ug54u4nrugyyb4g945n4gugtgtr045ygr45b4brgt" }
 * 
 * @apiParam {Object} data Request data
 * @apiParam {string} data.text Question text
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data.id Updated created answer's id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		id: 604
 *     	}
 *     }
 *
 *
 *
 * @apiError NotOwner User requesting the action is not object owner
 *
 * @apiErrorExample NotOwner:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "message": "User is trying to change data that doesn't belong to him",
 *       "status": 403,
 *       "code": 8,
 *       "data": null
 *     }
 * 
 *
 * @apiError NotAuthorized Not authorized for requested action
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Not authorized to execute requested action",
 *       "status": 401,
 *       "code": 16,
 *       "data": null
 *     }
 * 
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: AnswerUpdateRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	const answerId = parseInt(req.params.answer_id, 10);
	if (!validateNumber(answerId, true)) {
		closeRequest.genericError(res, "INVALID_OBJECT_ID");
		return;
	}
	const {body} = req;
	const error = validateRequest(body);
	if (error) {
		closeRequest.genericError(res, error);
		return;
	}
	const {
		text
	} = body.data;
	const userId = req.user.id;

	let updatedAnswerId: ?number;
	try {
		const answerData = await updateAnswer(
			req.user.id,
			answerId,
			text
		);
		if (!answerData) {
			closeRequest.genericError(res, "UNKNOWN_ERROR");
			return;
		}
		if (answerData.error !== null) {
			closeRequest.genericError(res, answerData.error);
			return;
		}
		updatedAnswerId = answerData.values.id;
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		id: updatedAnswerId
	};
	closeRequest.success(res, returnData);
}
