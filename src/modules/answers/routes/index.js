// @flow
"use strict";
const getAnswer = require("./handlers/getAnswer");
const createAnswer = require("./handlers/createAnswer");
const updateAnswer = require("./handlers/updateAnswer");
const deleteAnswer = require("./handlers/deleteAnswer");
const voteAnwer = require("./handlers/voteAnwer");

const {requireAuth} = require("../../auth/");

module.exports = (app: express$Application) => {
	app.get("/v1/answers/:answer_id", getAnswer);
	app.post("/v1/answers/:question_id", requireAuth, createAnswer);
	app.post("/v1/answers/:answer_id/edit", requireAuth, updateAnswer);
	app.post("/v1/answers/:id/vote", requireAuth, voteAnwer);
	app.delete("/v1/answers/:answer_id", requireAuth, deleteAnswer);
};
