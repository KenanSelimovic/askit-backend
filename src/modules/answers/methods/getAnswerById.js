// @flow
"use strict";
const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getAnswerById = require("../db/getAnswerById");

// flow types
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
import type {AnswerType} from "../types/answers.flow";
type AnswerResultType = {|
	error: null,
	values: {
		answer: AnswerType
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (answerId: number): Promise<AnswerResultType> => {
	// validate received data shape
	if (!validateNumber(answerId, true)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	let requestedAnswer;
	try {
		requestedAnswer = await getAnswerById(answerId);
		if (!requestedAnswer) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	// done, return
	return ({
		values: {
			answer: requestedAnswer
		},
		error: null
	});
};
