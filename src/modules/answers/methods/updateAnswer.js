// @flow
"use strict";
const {
	validateNumber,
	validateText
} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getAnswerById = require("../db/getAnswerById");
const updateAnswer = require("../db/updateAnswer");
const answersConfig = require("../../../config/config.js").config.answers;

// flow types
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type AnswerUpdateResultType = {|
	error: null,
	values: {
		id: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (
	userId: number,
	answerId: number,
	text: string
): Promise<AnswerUpdateResultType> => {
	// validate received data shape
	if (!validateNumber(userId) || !validateNumber(answerId)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}
	if (!validateText(text, answersConfig.text_min_len, answersConfig.text_max_len)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user is the answer owner
	let requestedAnswer;
	try {
		requestedAnswer = await getAnswerById(answerId);
		if (!requestedAnswer) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	if (requestedAnswer.user.id !== userId) {
		return ({
			values: null,
			error: "NOT_THE_OWNER"
		});
	}
	// update the answer
	let updatedAnswerId;
	try {
		updatedAnswerId = await updateAnswer(
			answerId,
			text
		);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	if (!updatedAnswerId) {
		crashLogger.log(
			"error",
			new Error(`Failed updating answer, answer_id ${answerId}, text ${text}`)
		)
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	return ({
		values: {
			id: updatedAnswerId
		},
		error: null
	});
};
