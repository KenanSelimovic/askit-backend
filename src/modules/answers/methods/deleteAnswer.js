// @flow
"use strict";
const {
	validateNumber,
	validateText
} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getAnswerById = require("../db/getAnswerById");
const deleteAnswer = require("../db/deleteAnswer");

// flow types
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type AnswerDeletionResultType = {|
	error: null,
	values: {
		id: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (
	userId: number,
	answerId: number
): Promise<AnswerDeletionResultType> => {
	// validate received data shape
	if (!validateNumber(userId) || !validateNumber(answerId)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user is the answer owner
	let requestedAnswer;
	try {
		requestedAnswer = await getAnswerById(answerId);
		if (!requestedAnswer) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	if (requestedAnswer.user.id !== userId) {
		return ({
			values: null,
			error: "NOT_THE_OWNER"
		});
	}
	// delete the answer
	let deletedAnswerId;
	try {
		deletedAnswerId = await deleteAnswer(
			answerId
		);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	if (!deletedAnswerId) {
		crashLogger.log(
			"error",
			new Error(`Failed deleting answer, answer_id ${answerId} by user ${userId}`)
		)
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	return ({
		values: {
			id: deletedAnswerId
		},
		error: null
	});
};
