// @flow
"use strict";
const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const {getUserById} = require("../../users/");
const storeAnswer = require("../db/storeAnswer");
const {answerInsertionSchema} = require("../types/validationSchemas");
const Joi = require("joi");
const {getQuestionById} = require("../../questions/");

// flow types
import type {AnswerInsertionType} from "../types/answers.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type AnswerCreationResultType = {|
	error: null,
	values: {
		id: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

module.exports = async (data: AnswerInsertionType): Promise<AnswerCreationResultType> => {
	// validate received data shape
	if (Joi.validate(data, answerInsertionSchema).error) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user in answer.user_id exists
	let requestedUser;
	try {
		const requestedUserData = await getUserById(data.user_id);
		if (requestedUserData.error !== null) {
			crashLogger.log(
				"error",
				new Error(`Failed retrieving user ${data.user_id} by id: ${JSON.stringify(requestedUserData)}`)
			)
			return ({
				values: null,
				error: "UNKNOWN_ERROR"
			});	
		}
		if (!requestedUserData.values.user) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// check if question in answer.question_id exists
	let requestedQuestion;
	try {
		const requestedQuestionData = await getQuestionById(data.question_id);
		if (requestedQuestionData.error !== null) {
			crashLogger.log(
				"error",
				new Error(`Failed retrieving question ${data.question_id} 
				by id: ${JSON.stringify(requestedQuestionData)}`)
			)
			return ({
				values: null,
				error: "UNKNOWN_ERROR"
			});	
		}
		if (!requestedQuestionData.values.question) {
			return ({
				values: null,
				error: "OBJECT_WITH_REQUESTED_ID_NOT_FOUND"
			});	
		}
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	// store the answer
	let storedAnswerId;
	try {
		storedAnswerId = await storeAnswer(data);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}

	if (!storedAnswerId) {
		crashLogger.log(
			"error",
			new Error(`Failed storing answer: ${JSON.stringify(data)}`)
		)
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});	
	}
	return ({
		values: {
			id: storedAnswerId
		},
		error: null
	});
};
