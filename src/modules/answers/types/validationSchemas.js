// @flow
"use strict";
const Joi = require("joi");
const answersConfig = require("../../../config/config").config.answers;

const {
	text_min_len,
	text_max_len
} = answersConfig;
const answerInsertionData = Joi.object().keys({
	user_id: Joi.number().integer().min(1),
	text: Joi.string().min(text_min_len).max(text_max_len),
	question_id: Joi.number().integer().min(1)
});
const answerFullData = Joi.object({
	id: Joi.number().integer().min(1).required(),
	text: Joi.string().min(text_min_len).max(text_max_len).required(),
	user: Joi.object({
		id: Joi.number().integer().min(1).required(),
		full_name: Joi.string().required().allow([null, ""]),
		profile_image_url: Joi.string().required().allow([null]),
	}).required(),
	question: Joi.object({
		id: Joi.number().integer().min(1).required()
	}).required(),
	time: Joi.date().timestamp('javascript').required(),
	votes: Joi.object({
		up: Joi.number().integer().min(0).required(),
		down: Joi.number().integer().min(0).required()
	}).required()
});

module.exports.answerInsertionSchema = answerInsertionData;
module.exports.answerSchema = answerFullData;
