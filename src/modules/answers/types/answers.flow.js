// @flow
"use strict";

export type AnswerInsertionType = {|
	user_id: number,
	text: string,
	question_id: number
|};
export type AnswerType = {|
	id: number,
	user: {
		id: number,
		full_name: string,
		profile_image_url: ?string
	},
	text: string,
	question: {
		id: number
	},
	votes: {
		up: number,
		down: number
	},
	time: number
|};
