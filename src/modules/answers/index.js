// @flow
"use strict";
const {answerSchema: _answerSchema} = require("./types/validationSchemas");
export type {AnswerType} from "./types/answers.flow";

module.exports.routes = require("./routes/");
module.exports.answerSchema = _answerSchema;