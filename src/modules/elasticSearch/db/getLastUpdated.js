// @flow
"use strict";
const db = require("../../db/dbConnection");

module.exports = async (): Promise<Array<number>> => {
	let searchStartDate = new Date();
	searchStartDate.setSeconds(searchStartDate.getSeconds() - 20);
	const timestampStart = searchStartDate.getTime();
	const mainQuery = `
		SELECT
			DISTINCT
			Q.post_id AS id
				FROM
					questions AS Q
						LEFT JOIN users AS U
						ON(Q.user_id=U.id)
							LEFT JOIN answers AS A
							ON(Q.post_id=A.question_id)
								LEFT JOIN votes AS V
								ON(Q.post_id=V.post_id)
									WHERE
										Q.updated_at > $1
										OR A.updated_at > $1
										OR V.updated_at > $1
										Or U.updated_at > $1;
	`;
	const questionsResult = await db.exec(mainQuery, [searchStartDate]);
	return questionsResult.rows.map(q => q.id);
};
