// @flow
"use strict";
const elasticsearch = require('elasticsearch');
const {crashLogger} = require("../logging/index");
const {elasticSearchUrl} = require("../../config/config");
const syncQuestions = require("./methods/syncQuestions");
const elasticSearchConfig = require("../../config/config").config.elasticSearch;
const createIndexIfNotExists = require("./methods/createIndex");

const user = process.env.ELASTIC_SEARCH_USER || "elastic";
const password = process.env.ELASTIC_SEARCH_PASSWORD;
if (!user || !password) {
	throw new Error(`Elastic search username or password not set`);
}

const client = new elasticsearch.Client({
	host: `${user}:${password}@${elasticSearchUrl}`,
	log: 'trace'
});

setInterval(async () => {
	await createIndexIfNotExists(client);
	syncQuestions(client);
}, elasticSearchConfig.refresh_interval);

module.exports.client = client;
module.exports.storeQuestionForSearch
	= require("./methods/storeQuestionForSearch");
module.exports.searchQuestions
	= require("./methods/searchQuestions").bind(null, client);
