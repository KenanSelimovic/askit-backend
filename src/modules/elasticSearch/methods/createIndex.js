// @flow
"use strict";
const redis = require("redis");
const redisClient = redis.createClient("redis://redis");
const {crashLogger} = require("../../logging/");

import type {client} from "elasticsearch";

let indexCreated = false;
module.exports = async (client: client) => {
	if (indexCreated) {
		return;
	}
	let indexExists;
	try {
		indexExists = await client.indices.exists({ index: "questions" });
	}
	catch (err) {
		crashLogger.log("error", err);
	}
	if (indexExists) {
		indexCreated = indexExists;
		return;
	}

	try {
		await client.indices.create({
			index: "questions",
			body: {
				"settings": {
					"analysis": {
						"filter": {
							"ngram_filter": {
								"type": "nGram",
								"max_gram": 8,
								"min_gram": 3
							},
							"truncate_filter": {
								"type": "truncate",
								"length": 3
							}
						},
						"analyzer": {
							"ngram_index_analyzer": {
								"tokenizer": "lowercase",
								"filter": ["ngram_filter"],
								"type": "custom"
							},
							"ngram_search_analyzer": {
								"tokenizer": "lowercase",
								"filter": ["truncate_filter"],
								"type": "custom"
							}
						}
					}
				},
				"mappings": {
					"Question": {
						"properties": {
							"text": {
								"type": "text",
								"analyzer": "ngram_index_analyzer",
								"search_analyzer": "ngram_search_analyzer"
							}
						}
					}
				}
			}
		})
		indexCreated = true;
	}
	catch (err) {
		return;
	}
};