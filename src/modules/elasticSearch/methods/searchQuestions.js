// @flow
"use strict";
const {crashLogger} = require("../../logging/");

import type {QuestionType} from "../../questions/index.flow";
import type {client} from "elasticsearch";
type SearchResultType = {|
	questions: Array<QuestionType>,
	total_count: number
|};

// eslint-disable-next-line immutable/no-mutation
module.exports = async (client: client, text: string, page: number, perPage: number): Promise<SearchResultType> => {
	try {
		const result = await client.search({
			index: 'questions',
			body: {
				query: {
					match_phrase: {
						text: {
							query: text
						}
					}
				}
			},
			size: perPage,
			from: (page - 1) * perPage
		});
		return ({
			questions: result.hits.hits.map(hit => hit._source),
			total_count: result.hits.total
		})
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			questions: [],
			total_count: 0
		})
	}
};
