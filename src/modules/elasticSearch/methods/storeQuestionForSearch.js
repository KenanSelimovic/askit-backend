// @flow
"use strict";
const {client} = require("..");
const {crashLogger} = require("../../logging/");

import type {QuestionType} from "../../questions/index.flow"

// eslint-disable-next-line immutable/no-mutation
module.exports = async (data: QuestionType): Promise<boolean> => {
	try {
		const result = await client.create({
			index: "questions",
			type: "Question",
			id: data.id,
			body: data
		});
		return true;
	}
	catch (err) {
		crashLogger.log("error", err);
		return false;
	}
};
