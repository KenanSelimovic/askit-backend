// @flow
"use strict";
const redis = require("redis");
const redisClient = redis.createClient("redis://redis");
const getLastUpdatedQuestions = require("../db/getLastUpdated");
const {crashLogger} = require("../../logging/");
const getQuestionsByIds = require("../../questions/methods/getQuestionsByIds");
const elasticSearchConfig = require("../../../config/config").config.elasticSearch;

import type {QuestionType} from "../../questions/index.flow";
import type {client} from "elasticsearch";

const generateUpdateQuery = (question: QuestionType) => {
	return ([
		{index: {_index: 'questions', _type: 'Question', _id: question.id}},
		question
	]);
};
const checkIfNeedUpdate = (): Promise<boolean> => {
	return new Promise((resolve, reject) => {
		redisClient.getset("elastic_last_updated", new Date().getTime(), (err, lastUpdated) => {
			if (err) {
				reject(err);
				return;
			}
			if (lastUpdated && new Date().getTime() - lastUpdated < elasticSearchConfig.refresh_interval / 2) {
				resolve(false);
				return;
			}
			resolve(true);
		});
	});
};

module.exports = async (client: client) => {
	let needToUpdate: boolean;
	try {
		needToUpdate = await checkIfNeedUpdate();
	}
	catch (err) {
		crashLogger.log("error", err);
		return;
	}
	if (!needToUpdate) {
		return;
	}
	let questions = [];
	try {
		const questionIds = await getLastUpdatedQuestions();
		const questionsData = await getQuestionsByIds(questionIds);
		if (questionsData.error) {
			crashLogger.log(
				"error",
				`Failed retrieving questions by ids: ${JSON.stringify(questionsData)}`
			);
			return;
		}
		questions = questionsData.values.questions;
	}
	catch (err) {
		crashLogger.log("error", err);
		return;
	}
	if (questions.length === 0) {
		return;
	}
	const bulkBody
		= questions
			.map(generateUpdateQuery)
			.reduce((acc, current) => [...acc, ...current], []);
	try {
		client.bulk({body: bulkBody});		
	}
	catch (err) {
		crashLogger.log("error", err)
	}
}