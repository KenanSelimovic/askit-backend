// @flow
"use strict";
const winston = require('winston');
const expressWinston = require('express-winston');

module.exports = (app: express$Application) => {
	expressWinston.requestWhitelist.push('body');
	expressWinston.bodyBlacklist.push('password');

	const rotatingLogsTransport = new winston.transports.DailyRotateFile({
		filename: 'logs/requestLogs/requests-',
		datePattern: 'yyyy-MM-dd',
		prepend: false
	});

	app.use(expressWinston.logger({
		transports: [
			rotatingLogsTransport
		],
		meta: true,
		msg: "HTTP {{req.method}} {{req.url}}",
		expressFormat: true
	}));
};
