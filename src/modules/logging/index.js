// @flow
"use strict";
const fs = require("fs");

const loggingTypes = ["requestLogs", "crashLogger", "dbLogger"];

/*
 * Function that needs to be called to prepare logging module for future
 * logging. It's supposed to be called only once, on server startup
 */
const initialise = (app: express$Application) => {
	if (!fs.existsSync("logs")) {
		fs.mkdirSync("logs", 0o774)
	}
	for (const dir of loggingTypes) {
		const path = "logs/" + dir;
		if (!fs.existsSync(path)) {
			fs.mkdirSync(path, 0o774)
		}
	}
	require("./requestLogs/requestLogs")(app);
};

const crashLogger = require("./crashLogger/crashLogger")();
const dbLogger = require("./dbLogger/dbLogger")();

module.exports.initialise = initialise;
module.exports.crashLogger = crashLogger;
module.exports.dbLogger = dbLogger;
