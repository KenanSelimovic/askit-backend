// @flow
"use strict";
const winston = require('winston');
const WinstonLogger = winston.Logger;

module.exports = (): WinstonLogger => {
	const dbLoggerTransport = new winston.transports.DailyRotateFile({
		filename: 'logs/dbLogger/db-queries-',
		datePattern: 'yyyy-MM-dd',
		prepend: false,
		handleExceptions: false
	});
	return new (winston.Logger)({
		transports: [
			dbLoggerTransport
		]
	});
};
