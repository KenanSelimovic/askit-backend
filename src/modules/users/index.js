// @flow
"use strict";
const getUserByEmail = require("./methods/getUserByEmail");
const getUserById = require("./methods/getUserById");

module.exports.getUserByEmail = getUserByEmail;
module.exports.getUserById = getUserById;
module.exports.routes = require("./routes");

export type {UserType} from "./types/users.flow";
