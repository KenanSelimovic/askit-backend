// @flow
"use strict";
const db = require("../../db/dbConnection");
const generateFullName = require("../../../helpers/generateFullName");

import type {UserStatisticsType} from "../types/users.flow";


// eslint-disable-next-line immutable/no-mutation
module.exports = async (
	page: number,
	perPage: number
): Promise<{users: Array<UserStatisticsType>, total_count: number}> => {
	const offset = page * perPage - perPage;
	const countAllQuery = `
		SELECT
			COUNT(*) AS total_count
				FROM
					users AS U
						WHERE U.is_deleted=false;
	`;
	const totalCountResult = await db.exec(countAllQuery, []);
	const totalCount = totalCountResult.rows[0].total_count;
	const mainQuery = `
		SELECT
			U.id,
			U.first_name,
			U.last_name,
			U.profile_image_url,
			(
				SELECT
					COUNT(*)
						FROM
							answers
								WHERE
									answers.user_id=U.id
			) AS num_answers,
			(
				SELECT
					COUNT(*)
						FROM
							questions
								WHERE
									questions.user_id=U.id
			) AS num_questions,
			(
				SELECT
					COUNT(*)
						FROM
							votes
								JOIN vote_types
								ON(votes.type=vote_types.id)
									JOIN questions 
									ON(votes.post_id=questions.post_id)
										WHERE
											vote_types.name='up'
												AND questions.user_id=U.id
			) AS num_likes
				FROM
					users AS U
						WHERE U.is_deleted=false
							ORDER BY num_answers DESC
								LIMIT $1
									OFFSET $2;
	`;
	const usersResult = await db.exec(mainQuery, [
		perPage,
		offset
	]);
	const users: Array<UserStatisticsType> = usersResult.rows.map((userInDb) => {
		const full_name = generateFullName(userInDb.first_name, userInDb.last_name);
		const user: UserStatisticsType = {
			id: userInDb.id,
			full_name,
			profile_image_url: userInDb.profile_image_url,
			num_answers: parseInt(userInDb.num_answers, 10),
			num_questions: parseInt(userInDb.num_questions, 10),
			num_likes: parseInt(userInDb.num_likes, 10)
		};
		return user;
	});
	return ({users, total_count: totalCount});
};
