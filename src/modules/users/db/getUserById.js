// @flow
"use strict";
const db = require("../../db/dbConnection");

// flow types
import type {UserType} from "../types/users.flow";

module.exports = async (id: number): Promise<?UserType> => {
	const mainQuery = `
		SELECT
			U.id,
			U.email,
			U.first_name,
			U.last_name,
			U.profile_image_url
				FROM
					users AS U
						WHERE
							U.id=$1
	`;
	const userResult = await db.exec(mainQuery, [id]);

	const userInDb = userResult.rows[0];
	if (!userInDb) {
		return null;
	}

	const user: UserType = {
		id: userInDb.id,
		first_name: userInDb.first_name,
		last_name: userInDb.last_name,
		email: userInDb.email,
		profile_image_url: userInDb.profile_image_url
	};

	return user;
};
