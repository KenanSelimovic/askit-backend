// @flow
"use strict";
const db = require("../../db/dbConnection");

// flow types
import type {
	UserType,
	ProfileUpdateDataType
} from "../types/users.flow";

module.exports = async (userId: number, data: ProfileUpdateDataType): Promise<number> => {
	const mainQuery = `
		UPDATE
			users
				SET
					first_name=COALESCE($1, first_name),
					last_name=COALESCE($2, last_name),
					email=COALESCE($3, email)
						WHERE id=$4
	`;
	await db.exec(mainQuery, [
		data.first_name,
		data.last_name,
		data.email,
		userId
	]);

	return userId;
};
