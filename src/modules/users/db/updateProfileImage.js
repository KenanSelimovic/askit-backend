// @flow
"use strict";
const db = require("../../db/dbConnection");

// flow types
import type {
	UserType,
	ProfileUpdateDataType
} from "../types/users.flow";

module.exports = async (userId: number, url: string): Promise<void> => {
	const mainQuery = `
		UPDATE
			users
				SET
					profile_image_url=$1
						WHERE id=$2
	`;
	await db.exec(mainQuery, [
		url,
		userId
	]);
};
