// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const updateProfile = require("../../methods/updateProfile");
const usersConfig = require("../../../../config/config.js").config.users;
const {
	validateEmail,
	validateText
} = require("../../../../helpers/validations/validations");
// flow types
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
import type {
	UserType,
	ProfileUpdateDataType
} from "../../types/users.flow";

type UpdateProfileBodyType = {
	data :ProfileUpdateDataType
}
declare class UpdateProfileRequestType extends express$Request {
	user: UserType,
	body: UpdateProfileBodyType
}

const validateRequest = (body: UpdateProfileBodyType): ?ErrorStringType => {
	const {
		data
	} = body;
	if (!data) {
		return ("DATA_PROPERTY_NOT_FOUND_IN_BODY");
	}
	const {
		email,
		first_name,
		last_name
	} = data;
	const {
		first_name_min_len,
		first_name_max_len,
		last_name_min_len,
		last_name_max_len
	} = usersConfig;
	if (!validateEmail(email)) {
		return ("INVALID_EMAIL");
	}
	if (first_name && !validateText(first_name, first_name_min_len, first_name_max_len)) {
		return (`INVALID_FIRST_NAME`);
	}
	if (last_name && !validateText(last_name, last_name_min_len, last_name_max_len)) {
		return (`INVALID_LAST_NAME`);
	}
	return null;
};

/**
 * @api {post} /v1/users/profile Update profile
 * @apiDescription Update user profile
 * @apiVersion 1.0.0
 * @apiName Get profile
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "a8313a5728bd419.a424e4dd6e503bc2ed73f08e9fe.7aa8f72e396970fdb49" }
 * 
 *
 * @apiParam {Object} data Update data.
 * @apiParam {string} data.email User's email.
 * @apiParam {string} [data.first_name] User's first name
 * @apiParam {string} [data.last_name] User's last name
 *
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success responses
 * @apiSuccess {Object} data  Main data object
 * @apiSuccess {Number} data.id  User id of the updated user
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *  	 	"status": 200,
 *			"message": "success",
 * 			"code": null,
 * 			"data": {
 * 		    	"id": 604
 * 		    }
 * 	}
 *
 * @apiError InvalidEmail Invalid email format received, for example string "abc"
 *
 * @apiErrorExample InvalidEmail:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid email received",
 *       "status": 422,
 *       "code": 1,
 *       "data": null
 *     }
 * 
 * @apiError InvalidPassword Received password is not in valid format (eg. too short)
 *
 * @apiErrorExample InvalidPassword:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid password received",
 *       "status": 422,
 *       "code": 3,
 *       "data": null
 *     }
 *
 * @apiError InvalidFirstName Received first name is not in valid format
 *
 * @apiErrorExample InvalidFirstName:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid first name received",
 *       "status": 422,
 *       "code": 4,
 *       "data": null
 *     }
 *
 * @apiError InvalidLastName Received last name is not in valid format
 *
 * @apiErrorExample InvalidLastName:
 *     HTTP/1.1 422 Unprocessable entity
 *     {
 *       "message": "Invalid last name received",
 *       "status": 422,
 *       "code": 5,
 *       "data": null
 *     }
 *
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 * @apiError NotAuthorized Authorization token is invalid
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *   	 "status": 401,
 *		 "message": "Not authorized",
 * 		 "code": null,
 *		 "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: UpdateProfileRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const userId = req.user.id;
	const error = validateRequest(req.body);
	if (error) {
		closeRequest.genericError(res, error);
	}
	const {
		first_name,
		last_name,
		email
	} = req.body.data;
	const profileUpdateData: ProfileUpdateDataType = {
		first_name,
		last_name,
		email
	};
	let updateData;
	try {
		updateData = await updateProfile(userId, profileUpdateData);
	}
	catch (err) {
		next(err);
		return;
	}
	if (updateData.error !== null) {
		closeRequest.genericError(res, updateData.error);
		return;
	}
	closeRequest.success(res, {id: updateData.values.id});
}
