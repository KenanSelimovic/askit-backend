// @flow
"use strict";
const db = require("../../../db/dbConnection");
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const uploadImage = require("../../methods/uploadImage");

// flow types
import type {UserType} from "../../../users/";
type ProfileUpdateRequestBodyType = {
	profile_image: File
};
declare class ProfileUpdateRequest extends Request {
	body: ProfileUpdateRequestBodyType,
	user: UserType
}


/**
* @api {post} /v1/users/image Upload profile image
* @apiDescription Upload profile image
* @apiVersion 1.0.0
* @apiName Upload profile image
* @apiGroup Users
*
* @apiParam {File} profile_image Profile image file (multipart/form-data)
*
* @apiSuccess {String} message  String saying "success"
* @apiSuccess {Number} status  Status 200
* @apiSuccess {Number} code  Code is null for success objects
* @apiSuccess {Object} data Response data
* @apiSuccess {String} data.url Uploaded image url
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*     	 	status: 200,
*	     	message: 'Profile update successful',
*		    code: null,
*    	    data: {
*				"url": "/images/459756894.png"	
*			}
*     }
*
* @apiError UnknownServerError Some unknown server error occured
*
* @apiErrorExample UnknownServerError:
*     HTTP/1.1 500 Internal Server Error
*     {
*       "message": "Unknown server error",
*       "status": 500,
*       "code": null,
*       "data": null
*     }
*
* @apiError InvalidType Received file is not an image
*
* @apiErrorExample InvalidType:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "Invalid file type provided",
*       "status": 422,
*       "code": 6,
*       "data": null
*     }
*
* @apiError FileSizeLimit Received file is over 1.5 MB
*
* @apiErrorExample FileSizeLimit:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "File size is over size limit",
*       "status": 422,
*       "code": 7,
*       "data": null
*     }
*
*/
// eslint-disable-next-line max-len
async function uploadProfileImage(req: ProfileUpdateRequest, res: express$Response, next: express$NextFunction): Promise<void> {
	const {user} = req;

	let uploadData;
	try {
		uploadData = await uploadImage(req, res, db);
	}
	catch (err) {
		next(err);
		return;
	}
	if (uploadData.error !== null) {
		closeRequest.genericError(res, uploadData.error);
		return;
	}
	closeRequest.success(res, {url: uploadData.values.url});
}
module.exports = uploadProfileImage;
