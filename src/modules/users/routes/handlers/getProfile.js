// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const getUserProfile = require("../../methods/getUserProfile");
const {
	validateNumber
} = require("../../../../helpers/validations/validations");
// flow types
import type {UserType} from "../../types/users.flow";

declare class GetProfileRequestType extends express$Request {
	user: UserType
}

/**
 * @api {get} /v1/users/profile Get profile
 * @apiDescription Retrieve user profile
 * @apiVersion 1.0.0
 * @apiName Get profile
 * @apiGroup Users
 *
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "a8313a5728bd419.a424e4dd6e503bc2ed73f08e9fe.7aa8f72e396970fdb49" }
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success responses
 * @apiSuccess {Object} data  Main data object
 * @apiSuccess {Object} data.profile  User's profile data
 * @apiSuccess {String} data.profile.first_name  First name
 * @apiSuccess {String} data.profile.last_name  Last name
 * @apiSuccess {String} data.profile.email User's email
 * @apiSuccess {String} data.profile.profile_image_url  Path to profile image - add it to server url!
 *
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *  	 	"status": 200,
 *			"message": "success",
 * 			"code": null,
 * 			"data": {
 * 		    	"profile": {
 * 					"id": 604,
 * 		    		"first_name": "Kenan",
 * 		    		"last_name": "Selimovic",
 * 					"email": "Kenanselimovic@gmail.com",
 *					"profile_image_url": "/images/y4y4545yy4454ytegr.jpg"
 * 				}
 * 		    }
 * 	}
 *
 *
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 * @apiError NotAuthorized Authorization token is invalid
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *   	 "status": 401,
 *		 "message": "Not authorized",
 * 		 "code": null,
 *		 "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: GetProfileRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const userId = req.user.id;
	let profileData;
	try {
		profileData = await getUserProfile(userId);
	}
	catch (err) {
		next(err);
		return;
	}
	if (profileData.error !== null) {
		closeRequest.genericError(res, profileData.error);
		return;
	}
	closeRequest.success(res, {profile: profileData.values.profile});
}
