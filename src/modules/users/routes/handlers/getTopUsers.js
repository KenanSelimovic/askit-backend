// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateNumber
} = require("../../../../helpers/validations/validations");
const getMostActiveUsers = require("../../methods/getMostActiveUsers");
const usersConfig = require("../../../../config/config.js").config.users;

// flow types
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
declare class GetUsersRequestType extends Request {
	user: UserType,
	query: {
		per_page?: string,
		page?: string
	}
}

/**
* @api {get} /v1/users/top Get top users
* @apiDescription Get most active users/users with most answers
* @apiVersion 1.0.0
* @apiName Get top users
* @apiGroup Questions
*
*
* @apiParam {Number} page Pagination page number, default to first page, page num. 1
* @apiParam {Number} per_page Number of users to return per pagination page
*
* @apiSuccess {String} message  String saying "success"
* @apiSuccess {Number} status  Status 200
* @apiSuccess {Number} code  Code is null for success objects
* @apiSuccess {Object} data  Data object
* @apiSuccess {Object[]} data.users  Array of users
* @apiSuccess {Number} data.users.id Question id
* @apiSuccess {String} data.users.full_name User full name
* @apiSuccess {String} data.users.profile_image_url User's profile image
* @apiSuccess {Number} data.users.num_questions Number questions user asked
* @apiSuccess {Number} data.users.num_likes Number of upvotes user received
* @apiSuccess {Number} data.users.num_answers Number answers user has given
*
* @apiSuccessExample Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "message": "success",
*       "status": 200,
*       "code": null,
*       "data": {
*       	"users"": [
*				{
*					"id": 1,
*					"full_name": "Jack Jones",
*					"profile_image_url": "/images/2454237t8.png",
*					"num_answers": 20,
*					"num_questions": 5,
*					"num_likes": 38
*				}
*			]
*       }
*     }
*
*
* @apiError InvalidPaginationPage Invalid pagination page received
*
* @apiErrorExample InvalidPaginationPage:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "Page number needs to be a valid positive integer",
*       "status": 422,
*       "code": 9,
*       "data": null
*     }
* @apiError InvalidPerPageValue Invalid number of items per page received
*
* @apiErrorExample InvalidPerPageValue:
*     HTTP/1.1 422 Unprocessable entity
*     {
*       "message": "per_page value needs to be a valid positive integer",
*       "status": 422,
*       "code": 10,
*       "data": null
*     }
*
* @apiError UnknownServerError Some unknown server error occurred
*
* @apiErrorExample Error-Response:
*     HTTP/1.1 500 Internal server error
*     {
*       "message": "Unknown server error",
*       "status": 500,
*       "code": null,
*       "data": null
*     }
*
* @apiError NotAuthorized Authorization token is invalid
*
* @apiErrorExample NotAuthorized:
*     HTTP/1.1 401 Not authorized
*     {
*   	 "status": 401,
*		 "message": "Not authorized",
* 		 "code": null,
*		 "data": null
*     }
*
*/
// eslint-disable-next-line max-len
module.exports = async(req: GetUsersRequestType, res: express$Response, next: express$NextFunction): Promise<void> => {
	const page = req.query.page ? parseInt(req.query.page, 10) : 1;
	const perPage
		= req.query.per_page
			? parseInt(req.query.per_page, 10)
			: usersConfig.top_users_per_page;

	if (page && !validateNumber(page, true)) {
		closeRequest.genericError(res, "PAGINATION_INVALID_PAGE");
		return;
	}
	if (perPage && !validateNumber(perPage, true)) {
		closeRequest.genericError(res, "PAGINATION_INVALID_PER_PAGE");
		return;
	}
	let usersData;
	try {
		usersData = await getMostActiveUsers(
			page,
			perPage
		);
		if (usersData.error !== null) {
			closeRequest.genericError(res, usersData.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		users: usersData.values.users,
		total_pages: usersData.values.totalPages,
		page
	};
	closeRequest.success(res, returnData);
}