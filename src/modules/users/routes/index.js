// @flow
"use strict";
const getProfile = require("./handlers/getProfile");
const updateProfile = require("./handlers/updateProfile");
const uploadProfileImage = require("./handlers/uploadProfileImage");
const getTopUsers = require("./handlers/getTopUsers");

const {requireAuth} = require("../../auth");

module.exports = (app: express$Application) => {
	app.get("/v1/users/profile", requireAuth, getProfile);
	app.get("/v1/users/top", getTopUsers);
	app.post("/v1/users/profile", requireAuth, updateProfile);
	app.post("/v1/users/image", requireAuth, uploadProfileImage);
};
