// @flow
"use strict";
const getUserById = require("./getUserById");
const {getUserVotes} = require("../../votes/");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {ProfileDataType} from "../types/users.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type ProfileReturnType = {|
	values: {
		profile: ?ProfileDataType
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (id: number): Promise<ProfileReturnType> => {
	if (!validateNumber(id, true)) {
		return ({
			values: null,
			error: "INVALID_OBJECT_ID"
		})
	}
	let userData;
	try {
		userData = await getUserById(id);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	if (userData.error !== null || !userData.values.user) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user id ${id}: ${JSON.stringify(userData)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	const userVotesData = await getUserVotes(id);
	if (userVotesData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user votes: ${JSON.stringify(userVotesData)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	const profile: ProfileDataType = Object.assign(
		{},
		userData.values.user,
		{
			votes: userVotesData.values.votes
		}
	);
	return ({
		error: null,
		values: {
			profile
		}
	});
};
