// @flow
"use strict";
const fs = require('fs');
const closeRequest = require("../../../helpers/http/responseStandardizer");
const multer = require('multer');
const usersConfig = require("../../../config/config").config.users;
const {crashLogger} = require("../../logging");
const updateUserImage = require("../db/updateProfileImage");
const {staticDir} = require("../../../config/config");

// flow types
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type ImageUploadReturnType = {|
	values: {
		url: string
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

const upload = multer({
	dest: staticDir + "/images",
	limits: {
		fileSize: usersConfig.image_max_size_bytes
	},
	fileFilter: (req, file, cb) => {
		if (file.mimetype !== 'image/jpg'
			&& file.mimetype !== 'image/jpeg'
			&& file.mimetype !== 'image/bmp'
			&& file.mimetype !== 'image/png') {
			return cb(new Error("Invalid file format"));
		}
		return cb(null, true);
	}
}).single("profile_image");

// eslint-disable-next-line immutable/no-mutation, max-len
module.exports = (req: express$Request, res: express$Response): Promise<ImageUploadReturnType> => {
	return new Promise((resolve, reject) => {
		upload(req, res, (uploadErr: Error): void => {
			if (uploadErr) {
				const errorMessage = uploadErr.message;				
				if (errorMessage === "Invalid file format") {
					resolve({
						values: null,
						error: "INVALID_FILE_TYPE"
					});
					return;
				}
				if (uploadErr.code === "LIMIT_FILE_SIZE") {
					resolve ({
						values: null,
						error: "FILE_SIZE_OVER_LIMIT"
					});
					return;
				}
				crashLogger.log("error", uploadErr);
				resolve ({
					values: null,
					error: "UNKNOWN_ERROR"
				});
			}
			if (!req.file) {
				resolve (({
					values: null,
					error: "INVALID_SHAPE"
				}));
			}
			const {
				originalname,
				path
			} = req.file;
			const fileExtension = originalname.slice(originalname.length - 4);
			let newFullPath;
			if (path.indexOf(fileExtension) === -1) {
				newFullPath = path + fileExtension;
				fs.renameSync(path, newFullPath);
			}
			else {
				newFullPath = path;
			}
		
			// images are stored in static/images, but are served
			// from /images, so we remove the static part
			const pathToReturn = newFullPath.replace(staticDir, "");
			updateUserImage(req.user.id, pathToReturn)
				.then(() => {
					resolve ({
						values: {
							url: pathToReturn
						},
						error: null
					});
				})
				.catch((err) => {
					crashLogger.log("error", err);
					resolve ({
						values: null,
						error: "UNKNOWN_ERROR"
					});
				})
			
		});
	});
};
