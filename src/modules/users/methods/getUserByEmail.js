// @flow
"use strict";
const getUserByEmail = require("../db/getUserByEmail");
const {crashLogger} = require("../../logging");

/*
 * Flow types
 */
import type {UserType} from "../types/users.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type UserReturnType = {|
	values: {
		user: ?UserType
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (email: string): Promise<UserReturnType> => {
	let user: ?UserType;
	try {
		user = await getUserByEmail(email);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			user
		},
		error: null
	});
};
