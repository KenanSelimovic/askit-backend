// @flow
"use strict";

const {validateNumber} = require("../../../helpers/validations/validations");
const {crashLogger} = require("../../logging");
const getMostActiveUsers = require("../db/getMostActiveUsers");
const usersConfig = require("../../../config/config.js").config.users;

// flow types
import type {UserStatisticsType} from "../types/users.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
type UserResultType = {|
	error: null,
	values: {
		users: Array<UserStatisticsType>,
		totalPages: number
	}
|}
|
{|
	error: ErrorStringType,
	values: null
|};

// eslint-disable-next-line immutable/no-mutation
module.exports = async (
	page: number,
	perPage: number
): Promise<UserResultType> => {
	if (!validateNumber(page)) {
		return ({
			error: "PAGINATION_INVALID_PAGE",
			values: null
		});
	}
	if (!validateNumber(perPage)) {
		return ({
			error: "PAGINATION_INVALID_PER_PAGE",
			values: null
		});
	}
	
	let usersData;
	try {
		usersData = await getMostActiveUsers(
			page,
			perPage
		);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			values: null,
			error: "UNKNOWN_ERROR"
		});
	}
	const totalPages = Math.ceil(
		(usersData.total_count) / perPage
	);
	return ({
		error: null,
		values: {
			totalPages,
			users: usersData.users
		}
	})
};
