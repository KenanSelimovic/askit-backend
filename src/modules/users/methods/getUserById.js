// @flow
"use strict";
const getUserById = require("../db/getUserById");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {UserType} from "../types/users.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type UserReturnType = {|
	values: {
		user: ?UserType
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (id: number): Promise<UserReturnType> => {
	if (!validateNumber(id, true)) {
		return ({
			values: null,
			error: "INVALID_OBJECT_ID"
		})
	}
	let user: ?UserType;
	try {
		user = await getUserById(id);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			user
		},
		error: null
	});
};
