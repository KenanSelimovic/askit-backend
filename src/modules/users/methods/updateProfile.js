// @flow
"use strict";
const Joi = require("joi");
const getUserById = require("./getUserById");
const updateProfile = require("../db/updateProfile");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");
const {profileUpdateData: profileUpdateDataSchema} = require("../types/validationSchemas");
const getUserByEmail = require("./getUserByEmail");

/*
 * Flow types
 */
import type {ProfileUpdateDataType} from "../types/users.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type ProfileUpdateReturnType = {|
	values: {
		id: number
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (
	userId: number,
	data: ProfileUpdateDataType
): Promise<ProfileUpdateReturnType> => {
	// validate received shape
	if (!validateNumber(userId, true)) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}
	if (Joi.validate(data, profileUpdateDataSchema).error) {
		return ({
			error: "INVALID_SHAPE",
			values: null
		});
	}

	// check if user exists
	let userData;
	try {
		userData = await getUserById(userId);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (userData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user id ${userId}: ${JSON.stringify(userData)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (!userData.values.user) {
		return ({
			error: "INVALID_OBJECT_ID",
			values: null
		});
	}

	// check if somebody is already using this email
	const sameEmailData = await getUserByEmail(data.email);
	if (sameEmailData.error !== null) {
		crashLogger.log(
			"error",
			new Error(`Failed retrieving user by email: ${JSON.stringify(sameEmailData.error)}`)
		);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}
	if (sameEmailData.values.user && sameEmailData.values.user.id !== userId) {
		return ({
			error: "DUPLICATE_EMAIL",
			values: null
		});
	}

	// everything ok, update
	try {
		await updateProfile(userId, data);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			id: userId
		},
		error: null
	});
};
