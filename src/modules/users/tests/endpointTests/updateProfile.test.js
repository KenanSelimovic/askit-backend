// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {profileData: profileDataSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");

const before = test;


const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("POST /profile successfully changes profile data", async (t: tape$Context) => {
	const data = generateRandomProfileData();
	const newData = generateRandomProfileData();

	const {token, id} = await registerAccountAndLogin(data);

	const unparsedProfileUpdateResponse = await fetch(`${baseUrl}/v1/users/profile`, {
		method: "POST",
		headers: {
			"Content-type": "application/json",
			authorization: token
		},
		body: JSON.stringify({data: newData})
	});
	const profileUpdateResponse = await unparsedProfileUpdateResponse.json();
	t.equal(
		profileUpdateResponse.status,
		200,
		"response json contains status 200"
	);
	
	// fetch profile to check if it has changes
	const unparsedProfileResponse = await fetch(`${baseUrl}/v1/users/profile`, {
		headers: {
			authorization: token
		}
	});
	const {data: {profile: updatedData}} = await unparsedProfileResponse.json();

	t.equal(updatedData.first_name, newData.first_name, "First name updated");
	t.equal(updatedData.last_name, newData.last_name, "Last name updated");
	t.equal(updatedData.email, newData.email, "Email updated");
	t.equal(updatedData.id, id, "Proper user id is returned");

	t.end();
});
