// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {profileData: profileDataSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");

const before = test;


const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /profile returns valid profile shape", async (t: tape$Context) => {
	const data = generateRandomProfileData();
	const {token, id} = await registerAccountAndLogin(data);

	const unparsedProfileResponse = await fetch(`${baseUrl}/v1/users/profile`, {
		headers: {
			authorization: token
		}
	});
	const profileResponse = await unparsedProfileResponse.json();
	t.equal(
		profileResponse.status,
		200,
		"response json contains status 200"
	);
	t.ok(profileResponse.data, "data object exists in response");
	t.notOk(
		Joi.validate(profileResponse.data.profile, profileDataSchema).error,
		"Profile data has proper shape"
	);
	t.end();
});
