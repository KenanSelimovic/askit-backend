// @flow
"use strict";

require("./getProfile.test.js");
require("./updateProfile.test.js");
require("./uploadProfileImage.test.js");
require("./getTopUsers.test.js");
