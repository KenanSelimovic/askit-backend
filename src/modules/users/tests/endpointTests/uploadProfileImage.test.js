// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../../../config/config");
const FormData = require("form-data");
const fs = require("fs");

const before = test;


const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("POST /users/image successfully changes profile data", async (t: tape$Context) => {
	const data = generateRandomProfileData();

	const {token, id} = await registerAccountAndLogin(data);

	const form = new FormData();
	form.append(
		'profile_image',
		fs.createReadStream(require("path").join(__dirname, "./test_image.jpg"))
	);
	const imageUploadResponse = await fetch(`${baseUrl}/v1/users/image`, {
		method: "POST",
		headers: {
			authorization: token
		},
		body: form
	});
	t.equal(imageUploadResponse.status, 200, "Image upload is successful");

	const unparsedProfileResponse = await fetch(`${baseUrl}/v1/users/profile`, {
		headers: {
			authorization: token
		}
	});
	const profileResponse = await unparsedProfileResponse.json();

	const profileImageInProfile = profileResponse.data.profile.profile_image_url;

	t.ok(profileImageInProfile, "Uploaded profile image is returned with the profile");

	const imageFetchResponse = await fetch(`${baseUrl}${profileImageInProfile}`);
	t.equal(imageFetchResponse.status, 200, "Uploaded image can be fetched");

	t.end();
});
