// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {userStatistics: userStatisticsSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");

const before = test;


before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /users returns proper user statistics shape", async (t: tape$Context) => {
	const usersResponse = await fetch(`${baseUrl}/v1/users/top`);
	t.equal(usersResponse.status, 200, "Response passes");	
	const parsedResponse = await usersResponse.json();
	parsedResponse.data.users.forEach(user => {
		t.notOk(
			Joi.validate(user, userStatisticsSchema).error,
			"User has proper shape"
		);
	});
	t.pass("Users are of valid shape");
	t.end();
});
test("GET /users/top returns users ordered by num answers", async (t: tape$Context) => {
	const usersResponse = await fetch(`${baseUrl}/v1/users/top`);
	const parsedResponse = await usersResponse.json();
	const {users} = parsedResponse.data;

	for (let i = 0; i < users.length - 1; ++i) {
		const currentUser = users[i];
		const nextUser = users[i + 1];
		if (currentUser.num_answers < nextUser.num_answers) {
			t.fail(`Invalid order,
				${JSON.stringify(currentUser)}
				comes before ${JSON.stringify(nextUser)}`);
		}
	}
	t.pass("Users are in proper order");
	t.end();
});
