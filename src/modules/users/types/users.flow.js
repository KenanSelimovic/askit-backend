// @flow
"use strict";
import type {VoteDataType} from "../../votes/";

export type UserType = {|
	id: number,
	first_name: string,
	last_name: string,
	email: string,
	profile_image_url: ?string
|};
export type ProfileDataType = {|
	id: number,
	first_name: string,
	last_name: string,
	email: string,
	profile_image_url: ?string,
	votes: Array<VoteDataType>
|};
export type ProfileUpdateDataType = {|
	first_name: string,
	last_name: string,
	email: string
|};
export type UserStatisticsType = {|
	id: number,
	full_name: string,
	profile_image_url: ?string,
	num_answers: number,
	num_questions: number,
	num_likes: number
|};
