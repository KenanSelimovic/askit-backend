// @flow
"use strict";
const Joi = require("joi");
const usersConfig = require("../../../config/config").config.users;
const {voteSchema} = require("../../votes/types/validationSchemas");

const {
	first_name_min_len,
	first_name_max_len,
	last_name_min_len,
	last_name_max_len,
	password_min_len,
	password_max_len
} = usersConfig;
const profileUpdateDataSchema = Joi.object().keys({
	first_name: Joi.string().min(first_name_min_len).max(first_name_max_len).optional().allow([""]),
	last_name: Joi.string().min(last_name_min_len).max(last_name_max_len).optional().allow([""]),
	email: Joi.string().email(),
	profile_image_url:  Joi.string().uri({allowRelative: true}).optional().allow(["", null])
});
const profileDataSchema = Joi.object().keys({
	id: Joi.number().integer().min(1).required(),
	first_name: Joi.string().min(first_name_min_len).max(first_name_max_len).optional().allow(["", null]),
	last_name: Joi.string().min(last_name_min_len).max(last_name_max_len).optional().allow(["", null]),
	email: Joi.string().email(),
	profile_image_url:  Joi.string().uri({allowRelative: true}).optional().allow(["", null]),
	votes: Joi.array().items(voteSchema).required()
});
const userSchema = Joi.object().keys({
	id: Joi.number().integer().min(1).required(),
	first_name: Joi.string().min(first_name_min_len).max(first_name_max_len).optional().allow(["", null]),
	last_name: Joi.string().min(last_name_min_len).max(last_name_max_len).optional().allow(["", null]),
	email: Joi.string().email(),
	profile_image_url:  Joi.string().uri({allowRelative: true}).optional().allow(["", null])
});
const userStatisticsSchema = Joi.object().keys({
	id: Joi.number().integer().min(1).required(),
	full_name: Joi.string().allow(["", null]),
	profile_image_url:  Joi.string().uri({allowRelative: true}).optional().allow(["", null]),
	num_answers: Joi.number().integer().min(0).required(),
	num_questions: Joi.number().integer().min(0).required(),
	num_likes: Joi.number().integer().min(0).required()
});
module.exports.profileUpdateData = profileUpdateDataSchema;
module.exports.profileData = profileDataSchema;
module.exports.user = userSchema;
module.exports.userStatistics = userStatisticsSchema;
