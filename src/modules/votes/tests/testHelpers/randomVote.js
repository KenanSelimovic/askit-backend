// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../../../config/config");

module.exports = async (token: string): Promise<number> => {
	// find a random question
	const questionsResponse = await fetch(`${baseUrl}/v1/questions`);
	const questionsData = await questionsResponse.json();

	if (!questionsData.data) {
		throw new Error(`Failed fetching latest questions`);
	}
	const randomQuestion = chance.pickone(questionsData.data.questions);

	const voteData = {
		vote_type: chance.pickone(["up", "down"])
	};
	const voteResponse = await fetch(`${baseUrl}/v1/questions/${randomQuestion.id}/vote`, {
		method: "POST",
		headers: {
			authorization: token,
			"Content-type": "application/json"
		},
		body: JSON.stringify({data: voteData})
	});
	const parsedVoteResponse = await voteResponse.json();
	if (voteResponse.status < 200 || voteResponse.status > 300) {
		throw new Error(`Failed voting for question: ${JSON.stringify(parsedVoteResponse)}`);
	}
	return randomQuestion.id;
}