// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const test = require("tape");
const Chance = require("chance");
const chance = new Chance();
const {voteSchema} = require("../../types/validationSchemas");
const Joi = require("joi");
const {baseUrl} = require("../../../../config/config");
const randomVote = require("../testHelpers/randomVote");

const before = test;

// flow types
import type {VoteDataType} from "../../types/votes.flow";
type OwnVotesResponseType = {|
	data: {
		votes: Array<VoteDataType>
	},
	message: string,
	code: null,
	status: 200
|}
|
{|
	data: null,
	message: string,
	code: number,
	status: number
|};

const generateRandomProfileData
	= require("../../../../helpers/testHelpers/generateRandomProfileData");
const registerAccountAndLogin
	= require("../../../../helpers/testHelpers/registerAccountAndLogin")

before("Setup env", async(t: tape$Context): Promise<void> => {
	const serverListening = await fetch(baseUrl);
	t.equal(serverListening.status, 200, "Server is listening");
	t.end();
});
test("GET /votes returns votes array", async (t: tape$Context) => {
	const credentials = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(credentials);

	// give one vote, so that we can check that exactly one is returned
	await randomVote(token);

	const votesResponse = await fetch(`${baseUrl}/v1/votes`, {
		headers: {
			authorization: token
		}
	});
	t.equal(votesResponse.status, 200, "Response passes");	
	const parsedResponse = await votesResponse.json();

	t.equal(
		parsedResponse.status,
		200,
		"response json contains status 200"
	);
	t.ok(parsedResponse.data, "data object exists in response");
	t.ok(
		Array.isArray(parsedResponse.data.votes),
		"votes array is returned"
	);
	t.equal(
		parsedResponse.data.votes.length,
		1,
		"Proper number of votes is returned"
	);
	t.end();
});
test("GET /votes returns proper votes shape", async (t: tape$Context) => {
	const credentials = generateRandomProfileData();
	const {token} = await registerAccountAndLogin(credentials);

	// give a few votes, so that we can check shape
	await randomVote(token);
	await randomVote(token);

	const votesResponse = await fetch(`${baseUrl}/v1/votes`, {
		headers: {
			authorization: token
		}
	});
	t.equal(votesResponse.status, 200, "Response passes");	
	const parsedResponse = await votesResponse.json();
	parsedResponse.data.votes.forEach(vote => {
		t.notOk(
			Joi.validate(vote, voteSchema).error,
			"Vote has proper shape"
		);
	});
	t.pass("Votes are of valid shape");
	t.end();
});
