// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {VoteInsertionType} from "../../votes/";

module.exports = async (data: VoteInsertionType): Promise<?number> => {
	const mainQuery = `
		INSERT
			INTO
				votes(
					post_id,
					user_id,
					type
				)
				VALUES(
					$1,
					$2,
					(
						SELECT
							id
								FROM
									vote_types
										WHERE
											name=$3
					)
				)
					RETURNING post_id AS id;
	`;
	const voteCreationResult = await db.exec(mainQuery, [
		data.post_id,
		data.user_id,
		data.type
	]);
	const insertedVoteId = voteCreationResult.rows[0].id;
	if (!insertedVoteId) {
		return null;
	}
	return insertedVoteId;
};
