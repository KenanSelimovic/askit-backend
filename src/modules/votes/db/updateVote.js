// @flow
"use strict";
const db = require("../../db/dbConnection");

import type {VoteInsertionType} from "../../votes/";

module.exports = async (postId: number, userId: number, data: VoteInsertionType): Promise<?number> => {
	const mainQuery = `
		UPDATE
			votes
				SET
					type=(SELECT id FROM vote_types WHERE name=$1)
						WHERE post_id=$2
							AND user_id=$3;
	`;
	await db.exec(mainQuery, [
		data.type,
		postId,
		userId
	]);
};
