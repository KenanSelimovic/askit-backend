// @flow
"use strict";
const db = require("../../db/dbConnection");

// flow types
import type {VoteDataType} from "../types/votes.flow";

module.exports = async (userId: number, postId: number): Promise<?VoteDataType> => {
	const mainQuery = `
		SELECT
			VT.name AS type,
			V.user_id,
			V.post_id AS post_id
				FROM
					votes AS V
						LEFT JOIN vote_types AS VT
						ON(V.type=VT.id)
							WHERE
								V.post_id=$1
									AND V.user_id=$2;
	`;
	const voteResult = await db.exec(mainQuery, [postId, userId]);
	const voteInDb = voteResult.rows[0];
	if (!voteInDb) {
		return null;
	}

	const vote: VoteDataType = {
		user: {
			id: voteInDb.user_id
		},
		type: voteInDb.type,
		post_id: postId
	};

	return vote;
};
