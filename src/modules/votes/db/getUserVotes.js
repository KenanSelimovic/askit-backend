// @flow
"use strict";
const db = require("../../db/dbConnection");

// flow types
import type {VoteDataType} from "../types/votes.flow";

module.exports = async (userId: number): Promise<Array<VoteDataType>> => {
	const mainQuery = `
		SELECT
			VT.name AS type,
			V.user_id,
			V.post_id AS post_id
				FROM
					votes AS V
						LEFT JOIN vote_types AS VT
						ON(V.type=VT.id)
							WHERE
								V.user_id=$1;
	`;
	const votesResult = await db.exec(mainQuery, [userId]);
	const votesInDb = votesResult.rows;

	const votes: Array<VoteDataType> = votesInDb.map(voteInDb => {
		const vote: VoteDataType = {
			user: {
				id: userId
			},
			type: voteInDb.type,
			post_id: voteInDb.post_id
		}
		return vote;
	})

	return votes;
};
