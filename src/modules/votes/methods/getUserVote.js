// @flow
"use strict";
const getUserVote = require("../db/getUserVote");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {VoteDataType} from "../types/votes.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type UserVoteReturnType = {|
	values: {
		vote: ?VoteDataType
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (userId: number, postId: number): Promise<UserVoteReturnType> => {
	if (!validateNumber(postId, true)) {
		return ({
			values: null,
			error: "INVALID_OBJECT_ID"
		})
	}
	let vote: ?VoteDataType;
	try {
		vote = await getUserVote(userId, postId);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			vote
		},
		error: null
	});
};
