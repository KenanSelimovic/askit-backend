// @flow
"use strict";
const getUserVotes = require("../db/getUserVotes");
const {crashLogger} = require("../../logging");
const {validateNumber} = require("../../../helpers/validations/validations");

/*
 * Flow types
 */
import type {VoteDataType} from "../types/votes.flow";
import type {ErrorStringType} from "../../../helpers/http/errorCodes";
export type UserVotesReturnType = {|
	values: {
		votes: Array<VoteDataType>
	},
	error: null
|}
|
{|
	values: null,
	error: ErrorStringType
|};

module.exports = async (userId: number): Promise<UserVotesReturnType> => {
	if (!validateNumber(userId, true)) {
		return ({
			values: null,
			error: "INVALID_OBJECT_ID"
		})
	}
	let votes: Array<VoteDataType>;
	try {
		votes = await getUserVotes(userId);
	}
	catch (err) {
		crashLogger.log("error", err);
		return ({
			error: "UNKNOWN_ERROR",
			values: null
		});
	}

	return ({
		values: {
			votes
		},
		error: null
	});
};
