// @flow
"use strict";
const Joi = require("joi");
const {answerSchema} = require("../../answers/types/validationSchemas");

const voteData = Joi.object({
	post_id: Joi.number().integer().min(1).required(),
	type: Joi.string().valid("up", "down").required(),
	user: Joi.object({
		id: Joi.number().integer().min(1).required()
	}).required()
});

module.exports.voteSchema = voteData;
