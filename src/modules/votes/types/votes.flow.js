// @flow
"use strict";

export type VoteType = "up" | "down";
export type VoteInsertionType = {|
	user_id: number,
	type: VoteType,
	post_id: number
|};
export type VoteDataType = {|
	post_id: number,
	type: VoteDataType,
	user: {
		id: number
	}
|};
