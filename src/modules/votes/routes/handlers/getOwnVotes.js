// @flow
"use strict";
const closeRequest = require("../../../../helpers/http/responseStandardizer");
const {
	validateText,
	validateNumber
} = require("../../../../helpers/validations/validations");
const getUserVotes = require("../../methods/getUserVotes");

// flow types
import type {UserType} from "../../../users/index";
import type {ErrorStringType} from "../../../../helpers/http/errorCodes";
declare class UserVotesRequest extends Request {
	user: UserType
}

/**
 * @api {get} /v1/votes Get user votes
 * @apiDescription Get votes (likes, dislikes) given by current user
 * @apiVersion 1.0.0
 * @apiName Get user votes
 * @apiGroup Votes
 * 
 * @apiHeader {String} Authorization Authorization token
 *
 * @apiHeaderExample {String} Request-Example:
 * { "authorization": "4ug54u4nrugyyb4g945n4gugtgtr045ygr45b4brgt" }
 * 
 *
 * @apiSuccess {String} message  String saying "success"
 * @apiSuccess {Number} status  Status 200
 * @apiSuccess {Number} code  Code is null for success objects
 * @apiSuccess {Object} data.id Newly created answer's id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *     	status: 200,
 *     	message: 'success',
 *     	code: null,
 *     	data: {
 *     		"votes": [
 *				{
 *					"user": {
 *					"id": 745
 *					},
 *					"type": "up",
 *					"post_id": 162
 *				},
 *				{
 *					"user": {
 *					"id": 745
 *					},
 *					"type": "up",
 *					"post_id": 159
 *				},
 *				{
 *					"user": {
 *					"id": 745
 *					},
 *					"type": "up",
 *					"post_id": 160
 *				}
 			]
 *     	}
 *     }
 *
 *
 * @apiError NotAuthorized Not authorized for requested action
 *
 * @apiErrorExample NotAuthorized:
 *     HTTP/1.1 401 Not authorized
 *     {
 *       "message": "Not authorized to execute requested action",
 *       "status": 401,
 *       "code": 16,
 *       "data": null
 *     }
 * 
 * @apiError UnknownServerError Some unknown server error occured
 *
 * @apiErrorExample UnknownServerError:
 *     HTTP/1.1 500 Internal server error
 *     {
 *       "message": "Unknown server error",
 *       "status": 500,
 *       "code": null,
 *       "data": null
 *     }
 *
 */
// eslint-disable-next-line max-len
module.exports = async (req: UserVotesRequest, res: express$Response, next: express$NextFunction): Promise<void> => {
	let votesData;
	try {
		votesData = await getUserVotes(
			req.user.id
		);
		if (votesData.error !== null) {
			closeRequest.genericError(res, votesData.error);
			return;
		}
	}
	catch (err) {
		next(err);
		return;
	}
	const returnData = {
		votes: votesData.values.votes
	};
	closeRequest.success(res, returnData);
}
