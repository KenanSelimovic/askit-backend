// @flow
"use strict";
const getOwnVotes = require("./handlers/getOwnVotes");

const {requireAuth} = require("../../auth/");

module.exports = (app: express$Application) => {
	app.get("/v1/votes", requireAuth, getOwnVotes);
};
