// @flow
"use strict";

// eslint-disable-next-line immutable/no-mutation
module.exports = (app: express$Application) => {
	// endpoint for checking if server is alive
	app.get("/", (req, res) => {
		res.end("ok");
	});

	// rest of routes
	require("./modules/answers").routes(app);
	require("./modules/auth").routes(app);		
	require("./modules/questions").routes(app);
	require("./modules/users").routes(app);
	require("./modules/votes").routes(app);
};
