// @flow
"use strict";

// eslint-disable-next-line immutable/no-mutation
module.exports = (timestamp: number) => {
	const dateObject = new Date(timestamp);
	return {
		year: dateObject.getUTCFullYear(),
		month: dateObject.getUTCMonth(),
		day: dateObject.getUTCDate()
	};
};