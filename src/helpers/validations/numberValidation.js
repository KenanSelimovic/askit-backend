// @flow
"use strict";

module.exports = (number: number, onlyPositive: ?boolean = false): boolean => {
	if (!number && number !== 0) {
		return false;
	}
	if (isNaN(parseFloat(number))) {
		return false;
	}
	if (!isFinite(number)) {
		return false;
	}
	if (onlyPositive && number < 0) {
		return false;
	}
	return true;
};
