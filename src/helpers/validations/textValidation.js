// @flow
"use strict";

module.exports = (text: string, minLength: ?number, maxLength: ?number): boolean => {
	if (!text) {
		return false;
	}
	if (typeof text !== "string") {
		return false;
	}
	if (maxLength && text.length > maxLength) {
		return false;
	}
	const textWithoutSpaces = text.replace(/\s/g, '');
	if (textWithoutSpaces.length === 0) {
		return false;
	}
	const textWithoutNumbers = textWithoutSpaces.replace(/\d/g, '');
	if (textWithoutNumbers.length === 0) {
		return false;
	}
	const textWithoutSpecialCharacters = textWithoutNumbers.replace(/\W/g, '');
	if (textWithoutSpecialCharacters.length === 0) {
		return false;
	}
	if (minLength && textWithoutSpecialCharacters.length < minLength) {
		return false;
	}
	return true;
};
