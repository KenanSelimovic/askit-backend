// @flow
"use strict";

module.exports = (text: string, minLength: ?number = 1, maxLength: ?number): boolean => {
	if (!text) {
		return false;
	}
	if (typeof text !== "string") {
		return false;
	}
	if (maxLength && text.length > maxLength) {
		return false;
	}
	const textWithoutSpaces = text.replace(/\s/g, '');
	if (minLength && textWithoutSpaces.length < minLength) {
		return false;
	}
	return true;
};
