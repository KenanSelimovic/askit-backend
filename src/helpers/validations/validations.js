// @flow
"use strict";

// just to make all validations importable from a single file

module.exports.validateEmail = require("./emailValidation");
module.exports.validateText = require("./textValidation");
module.exports.validatePassword = require("./passwordValidation");
module.exports.validateTimestamp = require("./timestampValidation");
module.exports.validateDateOfBirth = require("./dateOfBirthValidation");
module.exports.validateNumber = require("./numberValidation");
module.exports.validateGender = require("./genderValidation");
