// @flow
"use strict";

const validateTimestamp = require("./timestampValidation");

module.exports = (dateOfBirth: number, minAge?: number = 10): boolean => {
    if (!validateTimestamp(dateOfBirth)) {
        return false;
    }
    let dateToCompareTo = new Date();
    dateToCompareTo.setFullYear(dateToCompareTo.getFullYear() - minAge);

    if (dateToCompareTo.getTime() < dateOfBirth) {
    	return false;
	}
    return true;
};