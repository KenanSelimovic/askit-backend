// @flow
"use strict";

const validateNumber = require("./numberValidation");

module.exports = (timestamp: number, futureOnly: ?boolean): boolean => {
    if (!timestamp) {
        return false;
    }
    if (!validateNumber(timestamp)) {
        return false;
    }
    if (new Date(parseInt(timestamp, 10)).toString() === "Invalid Date") {
        return false;
    }
    if (futureOnly) {
        const currentDateTime = new Date().getTime();
        if (timestamp < currentDateTime) {
            return false;
        }
    }
    return true;
};