// @flow
"use strict";

module.exports = (gender: string): boolean => {
	return (gender === "female" || gender === "male");
};