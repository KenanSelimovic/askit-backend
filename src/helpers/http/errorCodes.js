// @flow
"use strict";
export type ErrorCodeObjectType = {
	code: number,
	status: number,
	message: string
};
export type ErrorStringType = $Keys<typeof errorCodesList>;
type ErrorCodesListType = {
	[ErrorStringType]: ErrorCodeObjectType
};

const errorCodesList: ErrorCodesListType = {
	INVALID_EMAIL: {
		code: 1,
		status: 422,
		message: "Invalid email received"
	},
	DUPLICATE_EMAIL: {
		code: 2,
		status: 422,
		message: "Email already exists in database"
	},
	INVALID_PASSWORD: {
		code: 3,
		status: 422,
		message: "Invalid password received"
	},
	INVALID_FIRST_NAME: {
		code: 4,
		status: 422,
		message: "Invalid first name received"
	},
	INVALID_LAST_NAME: {
		code: 5,
		status: 422,
		message: "Invalid last name received"
	},
	INVALID_FILE_TYPE: {
		code: 6,
		status: 422,
		message: "Invalid file type provided"
	},
	FILE_SIZE_OVER_LIMIT: {
		code: 7,
		status: 422,
		message: "File size is over size limit"
	},
	NOT_THE_OWNER: {
		code: 8,
		status: 403,
		message: `User is trying to change data that doesn't belong to him`
	},
	PAGINATION_INVALID_PAGE: {
		code: 9,
		status: 422,
		message: `Page number needs to be a valid positive integer`
	},
	PAGINATION_INVALID_PER_PAGE: {
		code: 10,
		status: 422,
		message: `per_page value needs to be a valid positive integer`
	},
	OBJECT_WITH_REQUESTED_ID_NOT_FOUND: {
		code: 11,
		status: 404,
		message: `Object with id specified in url not found`
	},
	DATA_PROPERTY_NOT_FOUND_IN_BODY: {
		code: 12,
		status: 422,
		message: "data property containing submission data not found in request body"
	},
	UNKNOWN_ERROR: {
		code: 13,
		status: 500,
		message: "Unknown error occurred"
	},
	OBJECT_ID_IN_URL_NOT_FOUND: {
		code: 14,
		status: 422,
		message: `Requested object id not found in url`
	},
	INVALID_SHAPE: {
		code: 15,
		status: 422,
		message: `Received object/type shape is invalid`
	},
	NOT_AUTHORIZED: {
		code: 16,
		status: 401,
		message: `Not authorized to execute requested action`
	},
	INVALID_OBJECT_ID: {
		code: 17,
		status: 422,
		message: `Requested object/resource id missing or invalid`
	},
	INVALID_QUESTION_TEXT: {
		code: 18,
		status: 422,
		message: `Requested question text missing/invalid/too long/too short`	
	},
	INVALID_VOTE_TYPE: {
		code: 19,
		status: 422,
		message: `Vote can be either "up" or "down"`	
	},
	INVALID_ANSWER_TEXT: {
		code: 20,
		status: 422,
		message: `Requested question text missing/invalid/too long/too short`	
	},
	FORBIDDEN: {
		code: 21,
		status: 403,
		message: `User is not allowed to perform requested action`	
	},
	INVALID_SEARCH_TEXT: {
		code: 22,
		status: 422,
		message: `Question text is either missing or invalid (eg. too short)`	
	}
};
module.exports = errorCodesList;
