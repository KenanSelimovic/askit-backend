// @flow
"use strict";
/* eslint max-len: 0 */
const errorCodes = require("./errorCodes");
const {crashLogger} = require("../../modules/logging");

import type {ErrorCodeObjectType, ErrorStringType} from "./errorCodes";

module.exports.genericError = (res: express$Response, errorString: ErrorStringType, data: ?Object = null) => {
	if (!errorCodes[errorString]) {
		crashLogger.log("error", `Non existent error string used: ${errorString}`);
	}
	const defaultError = {
		code: null,
		status: 500,
		message: "Unknown error occurred"
	};
	const errorObject: ErrorCodeObjectType = errorCodes[errorString] || defaultError;
	const status = errorObject.status;
	res.status(status).send({status, message: errorObject.message, code: errorObject.code, data}).end();
};
module.exports.unknownServerError = (res: express$Response, error: ?string = "Unknown server error", code: ?number) => {
	res.status(500).send({ status: 500, message: error, code, data: null}).end();
};
module.exports.notAuthorized = (res: express$Response, error: ?string = "Not authorized", code: ?number = null) => {
	res.status(401).send({ status: 401, message: error, code, data: null}).end();
};
module.exports.invalidRequest = (res: express$Response, error: ?string = "Request not formatted properly", code: ?number = null) => {
	res.status(422).send({ status: 422, message: error, code, data: null}).end();
};
module.exports.badRequest = (res: express$Response, error: ?string = "Bad request", code: ?number = null) => {
	res.status(400).send({ status: 400, message: error, code, data: null}).end();
};
module.exports.success = (res: express$Response, data?: ?Object = null, message?: string = "success", code?: ?number = null) => {
	res.status(200).send({ status: 200, message, code, data}).end();
};
module.exports.forbidden = (res: express$Response, error: ?string = "forbidden", code: ?number = null) => {
	res.status(403).send({ status: 403, message: error, code, data: null}).end();
};
module.exports.notFound = (res: express$Response, error: ?string = "not found", code: ?number = null) => {
	res.status(404).send({ status: 404, message: error, code, data: null}).end();
};