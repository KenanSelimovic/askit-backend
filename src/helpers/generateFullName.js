// @flow
"use strict";

/*
 * Both first and last name are optional,
 * so when trying to form full name (firstName + " " + lastName)
 * we want to avoid adding space if first name or last name are not present
 */
module.exports = (firstName?: string = "", lastName?: string = "") => {
	const padding = firstName && lastName ? " " : "";
	const fullName = firstName + padding + lastName;
	return fullName;
};
