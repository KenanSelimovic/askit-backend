// @flow
"use strict";
// eslint-disable-next-line max-len
module.exports = (number: number): number | string => number.toString().length === 2 ? number : "0" + number;