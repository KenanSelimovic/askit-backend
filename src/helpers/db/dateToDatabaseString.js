// @flow
"use strict";
const toTwoDigits = require("./toTwoDigits");

// receives date object and turns it into a date string
// for postgres query
module.exports = (date: Date): string => {
	return date.getUTCFullYear()
		+ "-"
		+ toTwoDigits(date.getUTCMonth() + 1)
		+ "-"
		+ toTwoDigits(date.getUTCDate())
		+ " "
		+ toTwoDigits(date.getUTCHours())
		+ ":"
		+ toTwoDigits(date.getUTCMinutes())
		+ ":"
		+ toTwoDigits(date.getUTCSeconds());
};
