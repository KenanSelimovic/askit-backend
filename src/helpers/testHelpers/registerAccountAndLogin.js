// @flow
"use strict";
const fetch = require("isomorphic-fetch");
const {baseUrl} = require("../../config/config");

module.exports = async(credentials: Object): Promise<{token: string, id: number}> => {
	const unparsedRegistrationResponse = await fetch(`${baseUrl}/v1/auth/register`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: credentials})
	});

	// now we can login

	const unparsedLoginResponse = await fetch(`${baseUrl}/v1/auth/login`, {
		method: "POST",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify({data: credentials})
	});
	const loginData = await unparsedLoginResponse.json();
	return {token: loginData.data.token, id: loginData.data.id};
};
