// @flow
"use strict";

type DifferencesResultType = {
	valid: boolean,
	message: ?string
};

module.exports = (
	expected: Object,
	received: Object,
	fields: Array<string>
): DifferencesResultType => {
	const unexpectedDifferences = fields.filter(key => {
		return expected[key] !== received[key];
	});
	if (unexpectedDifferences.length > 0) {
		return {
			valid: false,
			message: `Unexpected differences: ${unexpectedDifferences.join(", ")}`
		};
	}
	return {
		valid: true,
		message: null
	};
};