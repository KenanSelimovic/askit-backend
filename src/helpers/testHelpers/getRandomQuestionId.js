// @flow
"use strict";
const Chance = require("chance");
const chance = new Chance();
const {baseUrl} = require("../../config/config");

module.exports = async (): Promise<?number> => {
	const latestQuestionsResponse = await fetch(`${baseUrl}/v1/questions`);
	const latestQuestionsData = await latestQuestionsResponse.json();
	const latestQuestions = latestQuestionsData.data.questions;
	if (latestQuestions.length === 0) {
		// no questions in db yet
		return null;
	}
	const randomQuestionId = chance.pickone(latestQuestions).id;
	return randomQuestionId;
}