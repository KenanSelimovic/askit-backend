// @flow
"use strict";

const Chance = require("chance");
const chance = new Chance();

module.exports = () => {
	return {
		email: chance.email(),
		password: chance.string({length: 8}),
		first_name: chance.first(),
		last_name: chance.last()
	};
};