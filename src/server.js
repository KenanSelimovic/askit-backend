
// @flow
"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const cluster = require("cluster");
const helmet = require("helmet");
const {staticDir} = require("./config/config");
require('winston-daily-rotate-file');

if (cluster.isMaster) {
	const numWorkers = require('os').cpus().length;

	console.log('Master cluster setting up ' + numWorkers + ' workers...');

	for (let i = 0; i < numWorkers; i++) {
		cluster.fork();
	}

	cluster.on('online', worker => {
		console.log('Worker ' + worker.process.pid + ' is online');
	});

	cluster.on('exit', (worker, code, signal) => {
		cluster.fork();
	});
} else {
	/*
	 * If in dev environment, read config from .env file
	 * In production, this will have to be set manually, through something
	 * like AWS environment configuration
	 */
	if (process.env.NODE_ENV !== "production") {
		require('dotenv').config();
	}
	const port = process.env.PORT || 3000;

	/*
	 * Init express App
	 */
	const app = express();

	/*
	 * Setup security defaults
	 */
	app.use(helmet());
	
	/*
	 * Configure the app to use bodyParser
	 */

	// support url encoded bodies
	app.use(bodyParser.urlencoded({ extended: true }));

	// support json encoded bodies
	app.use(bodyParser.json());

	/*
     * Config server bodies and headers
     */
	app.use((req, res, next) => {
		res.header('Access-Control-Allow-Origin', "*");
		res.header('Access-Control-Allow-Methods', "GET, PUT, POST, PATCH, DELETE");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, X-HTTP-Method-Override," + " Accept, Authorization");
		next();
	});

	/*
     * Setup logging
     */
	require("./modules/logging").initialise(app);

	/*
	 * Initialise elasticsearch client
	 */
	require("./modules/elasticSearch");

	/*
     * Requiring route files
     */
	require("./routes")(app);

	/*
     * Setup error handling route
     */
	app.use((err, req, res, next) => {
		const { crashLogger } = require("./modules/logging/index");
		crashLogger.log("error", err);
		res.status(500).send('Something broke!');
	});

	/*
     * Finally, start listening
     */
	app.listen(port);
}