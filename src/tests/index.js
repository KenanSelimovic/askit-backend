"use strict";

require('winston-daily-rotate-file');
require("../modules/auth/tests/endpointTests/auth.test");
require("../modules/questions/tests/endpointTests/questions.test");
require("../modules/votes/tests/endpointTests/votes.test");
require("../modules/answers/tests/endpointTests/answers.test");