module.exports = {
	apps: [
		{
			name: "askit-backend",
			script: "./server.js",
			watch: false,
			env: {
				NODE_ENV: "staging"
			},
			env_production: {
				NODE_ENV: "production"
			}
		}
	]
};
